from .AMASignal.generator import Generator
from .AMASignal.plotter import Plotter, GeneralAxes
from .AMASignal.processing import Processing
from .AMASignal.signal import Signal
from .AMASignal.physical import Physical

from .AMASignal.exceptions import SignalException

# Register our custom axes
import matplotlib.projections as proj

proj.register_projection(GeneralAxes)
