AMASignal package
=================

Submodules
----------

AMASignal.control module
------------------------

.. automodule:: AMASignal.control
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.exceptions module
---------------------------

.. automodule:: AMASignal.exceptions
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.fft module
--------------------

.. automodule:: AMASignal.fft
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.filter module
-----------------------

.. automodule:: AMASignal.filter
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.generator module
--------------------------

.. automodule:: AMASignal.generator
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.physical module
-------------------------

.. automodule:: AMASignal.physical
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.plotter module
------------------------

.. automodule:: AMASignal.plotter
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.processing module
---------------------------

.. automodule:: AMASignal.processing
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.signal module
-----------------------

.. automodule:: AMASignal.signal
   :members:
   :undoc-members:
   :show-inheritance:

AMASignal.waveForms module
--------------------------

.. automodule:: AMASignal.waveForms
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: AMASignal
   :members:
   :undoc-members:
   :show-inheritance:
