============
Installing
============

You can download and install a development copy:

.. code-block:: shell

    git clone https://gitlab.com/abbts-projects/AMASignal.git
    cd AMASignal
    python setup.py develop



If you would like to permanently install it, use:

.. code-block:: shell

    python setup.py install
