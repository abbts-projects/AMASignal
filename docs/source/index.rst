.. AMASignals documentation master file, created by
   sphinx-quickstart on Sun Nov 17 20:05:49 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AMASignals's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installing

   examples/index

   AMASignal
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
