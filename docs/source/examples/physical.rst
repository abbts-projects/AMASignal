
========
Physical
========

-------------------------------------
Sample electrical frequency response
-------------------------------------

.. image:: ./img/RLC_Schaltung.jpg


.. code-block:: python

    from AMASignal import Signal, Generator, Physical
    import numpy as np
    import matplotlib.pyplot as plt

    sig = Generator.Sin(f=0, fEnd=10*100e3)

    R1 = Physical.Resistor(10, sig, name="R1")
    R2 = Physical.Resistor(10, sig, siPrefix="k", name="R2")
    L = Physical.Inductor(10, sig, siPrefix="m", name="L")
    C = Physical.Capacitor(47, sig, siPrefix="u", name="C")

    Z_R2C = 1 / ((1 / C) + (1 / R2))
    Z = R1 + L + Z_R2C
    F = Z_R2C / Z

    #sig.plot()
    #Z_R2C.plot()
    #Z.plot()

    dbSignal = F.dbSignal
    dbSignal.signalName = "$U_a/U_e [db]$"

    angleSignal = F.angleSignal
    angleSignal.signalName = "$Phasenverschiebung [°]$"

    _, axes = plt.subplots(2, 1, subplot_kw=dict(projection="GeneralAxes"))
    axes[0].plotMainSignal(dbSignal)
    axes[1].plotMainSignal(angleSignal)
    plt.show()