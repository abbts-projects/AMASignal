===================
Digilent WaveForms
===================

It's possible, to directly control and record from your Digilent Analog
Discovery.

Pleas note, that this requires you to have the SDK installed (comes with the
standard WaveForms installation):
https://reference.digilentinc.com/reference/software/waveforms/waveforms-3/start


Here is a sample snippet to get a Sawtooth out on CH W1. Replace the Serial
number with the one you have.
We will also record the data back over the first analog input.

.. code-block:: python

    from AMASignal import Signal,Generator
    from AMASignal.waveForms import WaveForms, AnalogWaveforms
    import time

    # Generate a Signal
    sig = Generator.Sawtooth(f=0.5, n_sp=WaveForms.MAX_SAMPLE_POINTS)

    # Get a new waveforms instance
    wf = WaveForms()

    # Get a device by ID
    device = wf.getDeviceBySN("210321A29A5B")

    # Configure the output of Analog CH 0
    wf.analog.configureOutput(0, sig.signal, 0.5, 2, 2, start=True)

    # Let's record some data
    points = 50000
    freq = 10000

    # Record
    inSig = wf.analog.record(channel=0, offset=0, frequency=freq, samples=points, vRange=AnalogWaveforms.VoltageRange.PM25)

    # Stop the output
    wf.analog.stopOutput(0)

    # Plot the recorded data
    inSig.plot()

    # Do an FFT
    fft = FFT()
    fftSig = fft.getFFT(inSig)
    fftSig.plot()



#############
More complex
#############

A more complex example: Generate a signal based on multiple sine waves. Feed
this signal into the wave generator of the Ananlog Discovery. There is a
physical resistor and capacitor connected to the output of the wavegen. The
scope is measuring the voltage across the capacitor.

After getting the data back from the scope, do a FFT of both signals (generated
signal and measured signal). Whichever signal has less frequencies, will be
chosen to find the frequencies in the other signal. Now get the signal strength
ath roughly the same frequency and compare them. based on the calculated
response of the circuit.

Again, replace the Serial number of the Analog Discovery with the one you have.

.. code-block:: python

    from AMASignal import Signal, Generator, FFT, WaveForms, AnalogWaveforms, Physical, Processing
    import matplotlib.pyplot as plt
    import time
    import numpy as np

    # Generate a few Signals
    Generator.DEFAULT_SAMPLE_POINTS = 2 ** 12
    count = 1
    sig0 = Generator.DC(f=100, offset=0.5, n=count).signal
    sig1 = Generator.Sin(f=100, amplitude=2, n=count, phase_deg=0).signal
    sig2 = Generator.Sin(f=1000, amplitude=0.5, n=count * 10, phase_deg=30).signal
    sig3 = Generator.Sin(f=2000, amplitude=0.25, n=count * 20, phase_deg=60).signal
    sig4 = Generator.Sin(f=5000, amplitude=0.75, n=count * 50, phase_deg=-45).signal

    combinedSig = sig0 + sig1 + sig2 + sig3 + sig4

    Signal.IO.writeCSV(combinedSig, "~/Desktop/tstSignal.csv")


    # Get a new waveforms instance
    wf = WaveForms()

    # Get a device by ID
    device = wf.getDeviceBySN("210321A29A5B")

    # Configure the output of Analog CH 0
    wf.analog.configureOutput(channel=0, signal=combinedSig, start=True)
    time.sleep(2)

    # Let's record some data
    points = 100000
    freq = 800000

    # Record
    inSig = wf.analog.record(channel=0, offset=0.5, frequency=freq, samples=points, vRange=AnalogWaveforms.VoltageRange.PM25)

    # Show the measured signal and the input
    combinedSig.addSignalsAsHelpers(inSig)
    combinedSig.plot()

    # Stop the output
    wf.analog.stopOutput(0)


    #inSig = Signal.IO.readCSV("~/Desktop/inSig.csv", timeAxis="Time_(s)")

    # Plot the recorded data
    fft = FFT()
    fftSigMeas = fft.getConditionedFFT(inSig, fLimit=8000)
    fftSig = fft.getConditionedFFT(combinedSig, window=1, fLimit=8000)

    # Plot the FFT
    fftSig.addSignalsAsHelpers(fftSigMeas)
    #fftSig.plot()

    # Get the max points from the measurement
    localMaxMeas = fftSigMeas.process(Processing.LocalMax())
    localMax = fftSig.process(Processing.LocalMax())

    localMax.addSignalsAsHelpers(localMaxMeas)
    #localMax.plot()

    # Filter where they have the same frequency
    if len(localMax.time) < len(localMaxMeas.time):
        a = localMax
        b = localMaxMeas
    else:
        a = localMaxMeas
        b = localMax

    # Rounding
    times = a.time
    bTimeIndicies = []

    for aVal in a.time:
        val = b.time[np.abs(b.time-aVal).argmin()]
        bTimeIndicies.append(np.where(b.time==val)[0][0])

    b.time = b.time[bTimeIndicies]
    b.signal = b.signal[bTimeIndicies]

    # Create the comparison signal
    uaue = np.log10(b.signal / a.signal) * 20
    uauesig = Signal("UA/UE", times, uaue)
    uauesig.type = Signal.Types.scatter

    # Create the physical circuit
    fRange = Generator.Sin(f=1, fEnd=1e6, n_sp=2**20)
    R = Physical.Resistor(10, fRange, siPrefix="k", name="R")
    C = Physical.Capacitor(100, fRange, siPrefix="n", name="C")
    circuit = R + C
    F = C.ZHistory / circuit

    # Get the DB signal and frequency response
    dbSignal = F.dbSignal
    dbSignal.signalName = "$U_a/U_e [db]$"

    angleSignal = F.angleSignal
    angleSignal.signalName = "$Phasenverschiebung [°]$"

    # Plot it all
    _, axes = plt.subplots(2, 1, subplot_kw=dict(projection="GeneralAxes"))
    axes[0].plotMainSignal(dbSignal)
    axes[0].plotMainSignal(uauesig)
    axes[1].plotMainSignal(angleSignal)
    plt.show()



Or a similar aproch as above, but now we measure the input and do the FFT based on that:


.. code-block:: python

    from AMASignal import Signal, Generator, FFT, WaveForms, AnalogWaveforms, Physical, Processing
    import matplotlib.pyplot as plt
    import time
    import numpy as np

    # Generate a few Signals
    Generator.DEFAULT_SAMPLE_POINTS = 2 ** 12
    count = 1
    sig0 = Generator.DC(f=100, offset=0.5, n=count).signal
    sig1 = Generator.Sin(f=100, amplitude=2, n=count, phase_deg=0).signal
    sig2 = Generator.Sin(f=1000, amplitude=0.5, n=count * 10, phase_deg=30).signal
    sig3 = Generator.Sin(f=2000, amplitude=0.25, n=count * 20, phase_deg=60).signal
    sig4 = Generator.Sin(f=5000, amplitude=0.75, n=count * 50, phase_deg=-45).signal

    combinedSig = sig0 + sig1 + sig2 + sig3 + sig4

    Signal.IO.writeCSV(combinedSig, "~/Desktop/tstSignal.csv")


    # Get a new waveforms instance
    wf = WaveForms()

    # Get a device by ID
    device = wf.getDeviceBySN("210321A29A5B")

    # Configure the output of Analog CH 0
    wf.analog.configureOutput(channel=0, signal=combinedSig, start=True)
    time.sleep(2)

    # Let's record some data
    points = 2**14
    freq = 800000

    # Record
    inSigUa, inSigUe = wf.analog.record(channels=(0,1), offset=0.5, frequency=freq, samples=points, vRange=AnalogWaveforms.VoltageRange.PM25)

    # Show the measured signal and the input
    combinedSig.addSignalsAsHelpers(inSigUa, inSigUe)
    combinedSig.plot()

    # Stop the output
    wf.analog.stopOutput(0)


    #inSig = Signal.IO.readCSV("~/Desktop/inSig.csv", timeAxis="Time_(s)")

    # Plot the recorded data
    fft = FFT()
    fftSigMeas = fft.getConditionedFFT(inSigUa, fLimit=8000)
    fftSig = fft.getConditionedFFT(inSigUe, window=1, fLimit=8000)

    # Plot the FFT
    fftSig.addSignalsAsHelpers(fftSigMeas)
    fftSig.plot()

    # Get the max points from the measurement
    localMaxMeas = fftSigMeas.process(Processing.LocalMax())
    localMax = fftSig.process(Processing.LocalMax())

    localMax.addSignalsAsHelpers(localMaxMeas)
    localMax.plot()

    # Filter where they have the same frequency
    if len(localMax.time) < len(localMaxMeas.time):
        a = localMax
        b = localMaxMeas
    else:
        a = localMaxMeas
        b = localMax

    # Rounding
    times = a.time
    bTimeIndicies = []

    for aVal in a.time:
        val = b.time[np.abs(b.time-aVal).argmin()]
        bTimeIndicies.append(np.where(b.time==val)[0][0])

    b.time = b.time[bTimeIndicies]
    b.signal = b.signal[bTimeIndicies]

    # Create the comparison signal
    uaue = np.log10(b.signal / a.signal) * 20
    uauesig = Signal("UA/UE", times, uaue)
    uauesig.type = Signal.Types.scatter

    # Create the physical circuit
    fRange = Generator.Sin(f=1, fEnd=1e6, n_sp=2**20)
    R = Physical.Resistor(10, fRange, siPrefix="k", name="R")
    C = Physical.Capacitor(100, fRange, siPrefix="n", name="C")
    circuit = R + C
    F = C.ZHistory / circuit

    # Get the DB signal and frequency response
    dbSignal = F.dbSignal
    dbSignal.signalName = "$U_a/U_e [db]$"

    angleSignal = F.angleSignal
    angleSignal.signalName = "$Phasenverschiebung [°]$"

    # Plot it all
    _, axes = plt.subplots(2, 1, subplot_kw=dict(projection="GeneralAxes"))
    axes[0].plotMainSignal(dbSignal)
    axes[0].plotMainSignal(uauesig)
    axes[1].plotMainSignal(angleSignal)
    plt.show()
