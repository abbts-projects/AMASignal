==================
Tangent Lines
==================

Here we are reading a csv, fetching the inflection point and plotting a visual
tangent line. as an example, the csv file read is done with pandas. There is
some file cleanup we need to do prior to reading the csv.

.. code-block:: python

    from AMASignal import Signal, Processing
    import pandas as pd
    from io import StringIO
    import re

    # Open the file
    with open("./OUT.csv") as fh:
        # Read and clean up
        file = fh.read()
        file = file.replace(" ", "")
        file = re.sub(r"(\d),(\d)", r"\1.\2", file)

    # Process
    sig = pd.read_csv(StringIO(file), sep="\t")

    Sigi = Signal("PT2", sig["Zeit"], sig["PT1T2"], sig["GENERATOR"])
    inf = Sigi.process(Processing.InflectionPoint(), Processing.Differential())
    Sigi.addSignalsAsHelpers(inf)

    tang = Sigi.process(Processing.TangentLine(inf[0]))
    Sigi.addSignalsAsHelpers(tang)

    Sigi.plot()
