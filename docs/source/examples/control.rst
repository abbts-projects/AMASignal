========
Control
========

########################
Linearization
########################

.. code-block:: python

    from AMASignal.control import Control
    from AMASignal.signal import Signal

    # Load the signal
    sigs = Signal.IO.readCSV("~/Desktop/OUT.CSV", timeAxis="Zeit", dialect=Signal.IO.CSVTabDialect)

    # Sigs Columns: Col1 = Time, Col2 = Jumps, Col3 = Out
    lin = Control.Linearization()
    linSig = lin.run(sigs[0], sigs[1])

    #linSig.plot()

    # Save
    Signal.IO.writeCSV(linSig, "~/Desktop/linearized.XY", dialect=Control.BORIS.BORISXYDialect)


#############################
PT1 Modeling from a response
#############################

.. code-block:: python

    from AMASignal.control import Control
    from AMASignal.signal import Signal

    # Load the signal
    sigs = Signal.IO.readCSV(
        "~/Desktop/PT1.CSV", timeAxis="Zeit", dialect=Signal.IO.CSVTabDialect
    )

    # Cutting out a jump at the start of my measurement file
    sigs[1].signal = sigs[1].signal[2:]
    sigs[1].time = sigs[1].time[2:]

    # Header: Col1 = Time, Col2 = Jumps, Col3 = Out
    pt1 = Control.PT1()
    pt1.calc(sigs[0], sigs[1])

    print(f"Kp: {pt1.Kp}, T1: {pt1.T1}")




#############################
PT2 Modeling from a response
#############################

.. code-block:: python

    from AMASignal.control import Control
    from AMASignal.signal import Signal

    # Load the signal
    sigs = Signal.IO.readCSV("~/Desktop/PT2OUT.CSV", timeAxis="Zeit", dialect=Signal.IO.CSVTabDialect)

    # Header: Col1 = Time, Col2 = Jumps, Col3 = Out
    pt2 = Control.PT2()
    pt2.calcFromSignal(sigs[0], sigs[1])

    print(f"Kp: {pt2.Kp}, Te: {pt2.Te}, Tb: {pt2.Tb}")





###################
BORIS
###################
Reading / resetting passwords on BORIS .sbl/.bsy files.

.. code-block:: python

    from AMASignal.control import Control

    pwd = Control.BORIS.readPassword("/Users/Birdimonster/Downloads/Aufgabe_2_1_STUD/Regelstrecke.SBL")
    Control.BORIS.unlockFile("/Users/Birdimonster/Downloads/Aufgabe_2_1_STUD/Regelstrecke.SBL")
