===========================
Step generator and saving
===========================

.. code-block:: python

    from AMASignal.generator import Generator
    from AMASignal.signal import Signal

    duration = 20
    stepCount = 5

    f = Generator.getFrequencyFor(duration, stepCount)
    steps = Generator.Steps(f=f, n=stepCount, amplitude=5, offset=5)
    sig = steps.calculate()

    sig.plot()

    Signal.IO.writeCSV(sig, "~/Desktop/test.csv")
