==================
Jupyter (iPhyton)
==================

Simply include the :code:`%matplotlib inline` before using the library as
normal:


.. code-block:: python

    %matplotlib inline

    from AMASignal import Signal, Generator

    sin = Generator.Sin()
    sin.plot()
