
========
FFT
========

.. code-block:: python

    from AMASignal import Signal,Generator, FFT

    # Generate a few Signals
    count = 2
    sig0 = Generator.DC(f=10, offset=1,n=count).signal
    sig1 = Generator.Sin(f=10, amplitude=3, n=count, phase_deg=0).signal
    sig2 = Generator.Sin(f=20, amplitude=0.75, n=count*2, phase_deg=45).signal
    sig3 = Generator.Sin(f=40, amplitude=1.5, n=count*4, phase_deg=30).signal
    sig4 = Generator.Sin(f=60, amplitude=0.5, n=count*6, phase_deg=-60).signal

    combinedSig = sig0 + sig1 + sig2 + sig3 + sig4

    # Do an FFT
    fft = FFT()
    fftSig = fft.getConditionedFFT(combinedSig, window=1, fLimit=100)
    fftSig.plot()
