==================
Generating signals
==================

----------------------
Get a list of signals
----------------------
.. code-block:: python

    from AMASignal import Generator

    signalTypes = Generator.getSignals()
    print(signalTypes)

    second = Generator.signalFromName(signalTypes[1])()
    second.plot()


-----------------------------
Getting a generator directly
-----------------------------

.. code-block:: python

    from AMASignal import Generator

    sig = Generator.Square()
    sig.n = 50
    sig.f = 100
    sig.plot()


-----------------------------------
Plotting all the available signals
-----------------------------------
.. code-block:: python

    from AMASignal import Generator
    import matplotlib.pyplot as plt

    signalTypes = Generator.getSignals()
    sigCount = len(signalTypes)

    fig, axs = plt.subplots(sigCount, 1)

    for i in range(sigCount):
        gene = Generator.signalFromName(signalTypes[i])(n_sp=2**12, f=10, n=20)

        axs[i].set_title(gene.signal.name)
        axs[i].plot(gene.signal.time, gene.signal.signal)

    fig.tight_layout(pad=0.5)
    plt.show()
