==================
Signal Processing
==================

.. code-block:: python

    from AMASignal import Signal, Processing

    signals = Signal.IO.readCSV(
        "./messung.csv", "Zeit[s]", dialect=Signal.IO.CSVCommaDialect
    )

    for signal in signals:
        sigs = signal.process(
            Processing.Max(),
            Processing.Min(),
            Processing.LocalMax(),
            Processing.LocalMin(),
            Processing.InflectionPoint(),
            Processing.Std(),
            Processing.Average(),
            Processing.Differential(n=2),
            Processing.Downsample(n=4),
            Processing.ZeroCrossings(method=Processing.ZeroCrossings.Method.exact, offset=2),
            Processing.MovingAverage(window=3, method=Processing.MovingAverage.Method.centered),
            Processing.Integral(method=Processing.Integral.Method.trapez),
            Processing.Interpolate(method=Processing.Interpolate.Method.cubic, name="Interpolated")
        )
        # Add the processed signals as helpers
        signal.addSignalsAsHelpers(sigs)

        # Finding named signals
        namedSignal = [sig for sig in sigs if "Interpolated" in sig.name][0]
        print(namedSignal)

        # Plot it
        signal.plot()



-----------------
Advanced example
-----------------

Reading a csv, getting the intersections and then saving them as a csv:

.. code-block:: python

    from AMASignal import Signal, Processing
    import matplotlib.pyplot as plt
    import numpy as np

    # Read the CSV
    meas = Signal.IO.readCSV(
        "./messung_klausurvorbereitung.csv", "Zeit_[s]", Signal.IO.CSVCommaDialect
    )

    # Create a new sbplot
    fig, ax = plt.subplots(1, 1, subplot_kw=dict(projection="GeneralAxes"))

    # Add all the signals to the subplot
    colours = ["g", "r"]
    styles = ["-", "--"]
    displacement = [0, 0.5]

    integrls = []

    for i, sig in enumerate(meas):
        # Each measurement is in [km/h] -> We need m/s -> 3600s/1000m -> 3.6
        sig.signal = sig.signal / 3.6

        # Process it
        prc = sig.process(Processing.Integral(method=Processing.Integral.Method.trapez))

        # Add a displacement
        prc[0].signal += displacement[i]

        # Add the integrals to a list
        integrls.append(prc[0])

        # Plot
        ax.plot(sig.time, sig.signal, label=sig.name, color=colours[i], ls=styles[0])
        ax.plot(sig.time, prc[0].signal, label=prc[0].name, color=colours[i], ls=styles[1])

    # Get the intersections
    intr = integrls[0].getIntersections(integrls[1])
    intr.signalName = "Kreuzungsort [m]"
    intr.timeName = "Kreuzungszeit [s]"

    # Save the intersections
    Signal.IO.writeCSV(intr, "./out.csv")

    # Plot the intersections
    ax.axvline(intr.time, label="Intersections", ymin=0.1, ymax=0.9)


    # Show the subplot
    ax.set_ylabel("Geschwindigkeit [m/s], Weg [m/s]")
    plt.legend()
    plt.show()
