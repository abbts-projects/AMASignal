============
CSV Handling
============

------------------
Reading CSV files
------------------

.. code-block:: python

    from AMASignal import Signal

    signals = Signal.IO.readCSV("./messung.csv", "Zeit[s]", dialect=Signal.IO.CSVWaveformsDialect)

    for signal in signals:
        signal.plot()



------------------
Writing CSV files
------------------

Saving signals to csv (This will save 40MB!)

.. code-block:: python

    from AMASignal import Signal, Generator

    sig = Generator.Sin(f=1, fEnd=10*100e3)
    Signal.IO.writeCSV(sig.signal, f"{sig.signal.name}.csv")

