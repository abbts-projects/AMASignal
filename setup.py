from setuptools import setup

setup(
    name="AMASignal",
    version="1.0.2",
    description="AMA Signals",
    author="Sean Blackburn",
    author_email="birdicode@gmail.com",
    packages=["AMASignal"],
    install_requires=["numpy", "matplotlib", "pandas", "scipy"],
    license="MIT",
)
