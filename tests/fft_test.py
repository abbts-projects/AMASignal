"""
Automated tests for FFT
"""

# 3rd Party imports
import unittest
import random
import numpy as np
import scipy.signal as scipySignal
import copy

# Custom imports
from AMASignal.generator import Generator
from AMASignal.fft import FFT
from AMASignal.signal import Signal


class Test_FFT(unittest.TestCase):
    def setUp(self):
        self.fft = FFT()

        # Create a new signal
        self.signal = [
            Generator.Sin().signal,
            # Generator.Sin(f=100, fEnd=300).signal,
            Generator.Sin(f=67, n=1000).signal,
            Generator.Square().signal,
        ]

    def test_ceilBase2(self):
        self.assertEqual(self.fft.ceilBase2(10), 16)
        self.assertEqual(self.fft.ceilBase2(16), 16)
        self.assertEqual(self.fft.ceilBase2(20), 32)

    def test_resampleSignal(self):
        for signal in self.signal:
            # Resample the signal
            reSignal = self.fft.resampleSignal(signal)

            # Check if the new time has the right element count
            self.assertEqual(len(reSignal.time), self.fft.ceilBase2(len(signal.time)))

        # TODO: Other checks

    def test_applyWindow(self):
        for signal in self.signal:
            # Get the signal length
            sigLen = len(signal.signal)

            # Get the windows
            windows = [
                np.hanning(sigLen),
                np.hamming(sigLen),
                np.blackman(sigLen),
                scipySignal.tukey(sigLen),
            ]

            for window in windows:
                # Apply
                applied = self.fft.applyWindow(signal, window)

                # Check
                np.testing.assert_allclose(applied.signal, signal.signal * window)

    def test_getDCOffset(self):
        # Loop each signal
        for signal in self.signal:
            # Get a offset
            offset = random.randrange(0, 5, 1)

            # Add the offset
            signal.signal += offset

            # get the offset
            calculatedOffset = self.fft.getDCOffset(signal)

            # Check
            self.assertAlmostEqual(calculatedOffset, offset, 4)

    def test_removeDCOffset(self):
        # Loop each signal
        for signal in self.signal:
            # Get a offset
            offset = random.randrange(1, 6, 1)

            # Add the offset
            copySignal = copy.deepcopy(signal)
            copySignal.signal += offset

            # get the offset
            calculatedOffsetSignal = self.fft.removeDCOffset(copySignal)

            # Check
            np.testing.assert_allclose(signal.time, calculatedOffsetSignal.time)
            np.testing.assert_allclose(
                signal.signal, calculatedOffsetSignal.signal, atol=1e-7
            )
            self.assertEqual(
                calculatedOffsetSignal.name, f"{signal.name} w/o DC offset"
            )
