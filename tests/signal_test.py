"""
Automated tests for Signal
"""

# 3rd Party imports
import unittest
import numpy as np
from io import StringIO
import csv
import os

# Custom imports
from AMASignal.generator import Generator
from AMASignal.signal import Signal


class Test_Signal(unittest.TestCase):
    def setUp(self):
        pass

    def test_getIntersections(self):
        # Check intersections with vertical lines
        # With horizontal lines
        pass

    def test_timedelta(self):
        # Get some values
        times_lin = np.linspace(0, 10, 20, endpoint=False)
        times_random = np.random.random_integers(0, 10, 20)

        signal = np.linspace(5, 10, 20)

        # Create a new signal
        sig_lin = Signal("test linear", times_lin, signal)
        sig_random = Signal("test random", times_random, signal)

        linTD = sig_lin.timedelta()
        linTDMean = sig_lin.timedelta(True)
        randomTD = sig_random.timedelta(True)

        self.assertRaises(Exception, sig_lin.timedelta())
        self.assertEqual(linTD, 0.5)
        self.assertEqual(linTDMean, 0.5)
        self.assertEqual(randomTD, np.mean(np.diff(times_random)))


class Test_SignalIO(unittest.TestCase):
    def setUp(self):
        self.standardCSV = "Time,Sig\n0.01,55.5\n0.02,22.7"
        self.standardCSVMultiColumn = "Time,Sig1,Sig2\n0.01,55.5,11.5\n0.02,22.7,88.9"
        self.standardNoHeaderCSV = "0.01,55.5\n0.02,22.7"

        self.fftMeasurementFile = "tests/data/fftMeasurement.csv"

    def test_readCSVWithDefaults(self):
        # Check no header
        signal = Signal.IO.readCSV(StringIO(self.standardCSV), timeAxis="Time")

        # Check
        np.testing.assert_allclose(signal.time, np.array([0.01, 0.02]))
        np.testing.assert_allclose(signal.signal, np.array([55.5, 22.7]))
        self.assertEqual("Sig", signal.name)

    def test_readCSVWithHeader(self):
        # Check no header
        signal = Signal.IO.readCSV(
            StringIO(self.standardCSV),
            timeAxis="Time",
            dialect=Signal.IO.CSVCommaDialect,
        )

        # Check
        np.testing.assert_allclose(signal.time, np.array([0.01, 0.02]))
        np.testing.assert_allclose(signal.signal, np.array([55.5, 22.7]))
        self.assertEqual("Sig", signal.name)

    def test_readCSVNoHeader(self):
        # Check no header
        signal = Signal.IO.readCSV(
            StringIO(self.standardNoHeaderCSV),
            timeAxis=0,
            dialect=Signal.IO.CSVCommaDialectWOHeader,
        )

        # Check
        np.testing.assert_allclose(signal.time, np.array([0.01, 0.02]))
        np.testing.assert_allclose(signal.signal, np.array([55.5, 22.7]))

    def test_readCSVCustomStyle(self):
        # Check no header
        customDialect = Signal.IO.CSVDialect(sep=":")
        data = StringIO("0.01:55.5\n0.02:22.7")
        signal = Signal.IO.readCSV(data, timeAxis=0, dialect=customDialect)

        # Check
        np.testing.assert_allclose(signal.time, np.array([0.01, 0.02]))
        np.testing.assert_allclose(signal.signal, np.array([55.5, 22.7]))

    def test_readCSVCompactSingle(self):
        # read the data
        compactSignal = Signal.IO.readCSV(StringIO(self.standardCSV), timeAxis="Time")
        signalList = Signal.IO.readCSV(
            StringIO(self.standardCSV), timeAxis="Time", compact=False
        )

        # Check
        self.assertIsInstance(compactSignal, Signal)
        self.assertIsInstance(signalList, list)

    def test_readCSVCompactMulti(self):
        # read the data
        multiSignalCompact = Signal.IO.readCSV(
            StringIO(self.standardCSVMultiColumn), timeAxis="Time"
        )
        multiSignal = Signal.IO.readCSV(
            StringIO(self.standardCSVMultiColumn), timeAxis="Time", compact=False
        )

        # Check
        self.assertIsInstance(multiSignalCompact, list)
        self.assertIsInstance(multiSignal, list)

    def test_readCSVFileNoHeader(self):
        # read the data
        fileSignal = Signal.IO.readCSV(
            self.fftMeasurementFile,
            timeAxis=0,
            dialect=Signal.IO.CSVCommaDialectWOHeader,
        )

        # Check
        self.assertEqual(len(fileSignal.time), 500)
        self.assertEqual(len(fileSignal.signal), 500)
        self.assertEqual(
            fileSignal.name, f"{os.path.basename(self.fftMeasurementFile)} (1)"
        )
