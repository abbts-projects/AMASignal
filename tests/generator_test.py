"""
Automated tests for Generator
"""

# 3rd Party imports
import unittest
import numpy as np
from io import StringIO
import csv
import os

# Custom imports
from AMASignal.generator import Generator
from AMASignal.signal import Signal


class Test_Generator(unittest.TestCase):
    def setUp(self):
        pass

    def test_durationToFrequency(self):
        self.assertEqual(Generator.durationToFrequency(1), 1)
        self.assertEqual(Generator.durationToFrequency(10), 0.1)
        self.assertEqual(Generator.durationToFrequency(50), 0.02)

        with self.assertRaises(TypeError):
            Generator.durationToFrequency("a")

    def test_periodsInTime(self):
        self.assertEqual(Generator.periodsInTime(1, 1), 1)
        self.assertEqual(Generator.periodsInTime(10, 1), 10)
        self.assertEqual(Generator.periodsInTime(20, 0.5), 10)
        self.assertEqual(Generator.periodsInTime(10, 50), 500)

        with self.assertRaises(TypeError):
            Generator.periodsInTime("a", 1)

        with self.assertRaises(TypeError):
            Generator.periodsInTime(1, "a")

        with self.assertRaises(TypeError):
            Generator.periodsInTime("a", "b")

    def test_getFrequencyFor(self):
        self.assertEqual(Generator.getFrequencyFor(10, 10), 1)
        self.assertEqual(Generator.getFrequencyFor(20, 5), 0.25)
        self.assertEqual(Generator.getFrequencyFor(20, 50), 2.5)
        self.assertAlmostEqual(Generator.getFrequencyFor(20.8, 5.3), 0.2548077)

        with self.assertRaises(TypeError):
            Generator.getFrequencyFor("a", 1)

        with self.assertRaises(TypeError):
            Generator.getFrequencyFor(1, "a")

        with self.assertRaises(TypeError):
            Generator.getFrequencyFor("a", "b")
