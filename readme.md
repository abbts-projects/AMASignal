# AMA Signal

Documentation: [https://abbts-projects.gitlab.io/AMASignal/](https://abbts-projects.gitlab.io/AMASignal/)

## Installation
```shell
git clone https://gitlab.com/abbts-projects/AMASignal.git
cd AMASignal
pip install -r requirements.txt
python setup.py develop
```
