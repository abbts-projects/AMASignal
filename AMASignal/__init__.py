from .generator import Generator
from .plotter import Plotter, GeneralAxes
from .processing import Processing
from .signal import Signal
from .physical import Physical
from .fft import FFT
from .control import Control
from .filter import Filter
from .waveForms import WaveForms, AnalogWaveforms, DigitalWaveforms

from .exceptions import SignalException

# Register our custom axes
import matplotlib.projections as proj

proj.register_projection(GeneralAxes)
