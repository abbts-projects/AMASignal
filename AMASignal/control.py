# Type checking
from __future__ import annotations
from typing import Union, TYPE_CHECKING, List, Dict, NamedTuple, Optional, cast, Tuple

if TYPE_CHECKING:
    pass

# 3rd party imports
import pandas as pd
import numpy as np
from io import StringIO
import re
from collections import namedtuple
from enum import Enum, auto
import os
import ctypes

# Custom imports
from . import Signal, Processing


class Control:
    class P:
        pass

    class PT1:
        def __init__(self):
            self.Kp = 0
            self.T1 = 0

        def calcFromSignal(self, jump: Signal, PT1Signal: Signal) -> Signal:
            """
            [summary]
            
            Parameters
            ----------
            jump : Signal
                [description]
            PT1Signal : Signal
                [description]

            Returns
            -------
            Signal
                [description]
            
            Raises
            ------
            Exception
                [description]
            """
            # Get the last value of the signal (average of 5 values)
            finalState = np.mean(PT1Signal.signal[-6:-1])
            initialState = np.mean(PT1Signal.signal[0:5])

            finalStateJump = np.mean(jump.signal[-6:-1])
            Kp = finalState / finalStateJump

            # Level everything out
            self.offset = initialState
            PT1Signal.signal -= self.offset
            finalState -= self.offset

            initialState = 0  # We are back down to 0

            # Process the signal
            diff = PT1Signal.process(Processing.Differential())

            # Add them as helpers for the plot
            PT1Signal.addSignalsAsHelpers(diff)

            # Get the max of the diff, this is where we will have our tangent
            diffMax = np.max(diff.signal)
            diffIndicies = np.where(diff.signal == diffMax)

            if len(diffIndicies[0]) > 1:
                # TODO: Added 10 as measurement at the start does not seem to be accurate
                diffIndicies = np.array([diffIndicies[0][0] + 10])

            diffTime = diff.time[diffIndicies]

            signalVal = PT1Signal.signal[np.where(PT1Signal.time == diffTime)]

            diffP = Signal(
                "Diff p",
                diffTime,
                np.array([signalVal]),
                signalType=Signal.Types.scatter,
            )

            # Get the tangent based on the inflection point
            tang = PT1Signal.process(Processing.TangentLine(diffP))
            tang = cast(List[Signal], tang)
            PT1Signal.addSignalsAsHelpers(tang, diffP)

            # Create a line
            finalStateSignal = Signal(
                "Final State",
                PT1Signal.startFinishTimes,
                finalState,
                signalType=Signal.Types.hline,
            )
            PT1Signal.addSignalsAsHelpers(finalStateSignal)

            # Get the intersections with the tangent line
            finalIntersection = tang.getIntersections(finalStateSignal)
            PT1Signal.addSignalsAsHelpers(finalIntersection)

            try:
                finalIntersectionTime = finalIntersection.time[0]

            except:
                raise Exception("The tangent does not cross the final value!")

            self.Kp = Kp
            self.T1 = finalIntersectionTime - diffTime

            # Return the signal
            return PT1Signal

    class PT2:
        def __init__(self):
            self.Kp = 1
            self.Te = 0
            self.Tb = 0
            self.offset = 0

        def calcFromSignal(
            self,
            jump: Signal,
            PT2Signal: Signal,
            averageValues: int = 5,
            showDiffSignal: bool = False,
        ) -> Signal:
            """
            [summary]
            
            Parameters
            ----------
            jump : Signal
                [description]
            PT2Signal : Signal
                [description]
            averageValues : int
                How many values to average together
            showDiffSignal : bool
                If we should add the diff signal as a helper signal

            Returns
            -------
            Signal
                [description]
            
            Raises
            ------
            Exception
                [description]
            """
            # Get the last value of the signal (average of 5 values)
            finalState = np.mean(PT2Signal.signal[-(averageValues + 1) : -1])
            initialState = np.mean(PT2Signal.signal[0:averageValues])

            # And now of the jump. The jump is a perfect signal!
            finalStateJump = jump.signal[-1]
            initialStateJump = jump.signal[1]

            # Level everything out
            self.offset = initialState

            zeroOffsetPT2 = Signal(
                "Zero offset PT2", PT2Signal.time, PT2Signal.signal - self.offset
            )

            # Process the signal
            diff = cast(List[Signal], zeroOffsetPT2.process(Processing.Differential()))

            if showDiffSignal:
                # Add them as helpers for the plot
                PT2Signal.addSignalsAsHelpers(diff)

            # Get the max of the diff, this is where we will have our tangent
            diffMax = np.max(diff.signal)
            diffTime = diff.time[np.where(diff.signal == diffMax)]
            signalVal = PT2Signal.signal[np.where(PT2Signal.time == diffTime)]

            diffP = Signal(
                "Diff p",
                diffTime,
                np.array([signalVal]),
                signalType=Signal.Types.scatter,
            )

            # Get the tangent based on the inflection point
            tang = PT2Signal.process(Processing.TangentLine(diffP))
            tang = cast(List[Signal], tang)
            PT2Signal.addSignalsAsHelpers(tang, diffP)

            # Create a line
            finalStateSignal = Signal(
                "Final State",
                PT2Signal.startFinishTimes,
                finalState,
                signalType=Signal.Types.hline,
            )
            initialStateSignal = Signal(
                "Initial State",
                PT2Signal.startFinishTimes,
                initialState,
                signalType=Signal.Types.hline,
            )

            PT2Signal.addSignalsAsHelpers(finalStateSignal, initialStateSignal)

            # Get the intersections with the tangent line
            finalIntersection = tang.getIntersections(finalStateSignal)
            initialIntersection = tang.getIntersections(initialStateSignal)

            PT2Signal.addSignalsAsHelpers(finalIntersection, initialIntersection)

            # Get the start time of the jump
            diffGen = jump.process(Processing.Differential())

            # Get all the jumps
            jumped = np.roll(diffGen.signal, -1)[:-1] - diffGen.signal[:-1] > 0

            try:
                jumpTimes = np.extract(jumped, diffGen.time[:-1])
                jumpTime = jumpTimes[0]

            except Exception as e:
                raise Exception("Could not find a signal jump!", e)

            # Try and get some values
            try:
                initIntersectionTime = initialIntersection.time[0]

            except:
                raise Exception("The tangent does not cross the initial value!")

            try:
                finalIntersectionTime = finalIntersection.time[0]

            except:
                raise Exception("The tangent does not cross the final value!")

            deltaInitial = abs(finalStateJump - initialStateJump) 
            deltaFinal = abs(finalState - initialState)

            self.Kp = deltaInitial / deltaFinal 
            self.Te = initIntersectionTime - jumpTime
            self.Tb = finalIntersectionTime - initIntersectionTime

            # Return the signal
            return PT2Signal

    class PT2S:
        pass

    class PTt:
        pass

    class I:
        pass

    class D:
        pass

    class DT1:
        pass

    class BORIS:
        BORISXYDialect = Signal.IO.CSVDialect(sep="\t", encoding="cp1250")

        @staticmethod
        def readPassword(path: str):
            """
            Only meant to recover lost passwords from a boris file!
            """
            """
            Table of passwords and the resulting hex value

            PWD = INT   = HEX   = ASCII INT
            0   = -49   = CF    = 48
            1   = -50   = CE    = 49
            2   = -51   = CD    = 50
            3   = -52   = CC    = 51
            4   = -53   = CB    = 52
            5   = -54   = CA    = 53
            6   = -55   = C9    = 54
            7   = -56   = C8    = 55
            8   = -57   = C7    = 56
            9   = -58   = C6    = 57
            :   = -59           = 58
            ;   = -60           = 59

            $   = -37   =       = 36

            a   = -98   = 9E    = 97
            b   = -99   = 9D    = 98
            """
            # open the file
            with open(path, "rb") as fh:
                content = fh.readlines()

            # Find the content [File Access]
            for lineNr, line in enumerate(content):
                if "[File Access]" in line.decode("UTF-8"):
                    break

            # Get the interesting part
            pwdLine = content[lineNr + 1][4:-2]
            pwdTXT = ""

            # Loop each char
            for char in pwdLine:
                pwdTXT += chr((ctypes.c_int8(char).value * -1) - 1)

            # Return the password
            return pwdTXT

        @staticmethod
        def unlockFile(path: str):
            """
            Unlock any "locked" boris file. This is meant for recovery!
            For example, you set the password to: 😋
            You will never ever be able to recover the file again, as 
            BORIS doesn't encode it correctly!
            
            Parameters
            ----------
            path : str
                The path to the boris file
            """
            # open the file
            with open(path, encoding="utf-8", errors="ignore") as fh:
                content = fh.readlines()

            # Find the content [File Access]
            for lineNr, line in enumerate(content):
                if "[File Access]" in line:
                    break

            # Set the next line
            content[lineNr + 1] = "0   \n"

            # Save it back
            root, ext = os.path.splitext(path)
            newPath = f"{root}-unlocked{ext}"

            with open(newPath, "w+") as fh:
                fh.writelines(content)

            return True

    class Linearization:
        def __init__(self):
            pass

        def run(self, jumps: Signal, out: Signal) -> Signal:
            """
            Linearize a signal based on it's response of a set of jumps
            
            Warning: This will fail, if the jumps signal does not have
            the same amount of points as the out signal!

            Parameters
            ----------
            jumps : Signal
                The jump signal
            out : Signal
                The out signal of the model (???)
            
            Returns
            -------
            Signal
                The linearized signal
            """
            # Do some rounding on the signal
            roundedJumpsSignal = np.around(jumps.signal, 4)

            # Get the jumps, shift to get the value just before the jump
            willJump = np.diff(roundedJumpsSignal) > 0
            willJump = np.roll(willJump, -1)
            willJump[-1] = True
            # willJumpTimes = jumps.time[np.where(willJump == True)]

            # Get the last measured value
            # This is the "max" value = 100%
            lastValue = np.average(out.signal[-5:-1])

            # Map the signal to 0-100%
            normalizedSignal = out.signal / lastValue * 100

            # Get the axis
            x_norm = normalizedSignal[np.where(willJump == True)]
            y = roundedJumpsSignal[np.where(willJump == True)]

            # TODO: Fix for duplicate values in x!

            # Create the signal
            sig = Signal("Linearization", x_norm, y)

            return sig

    class PID:
        class ControlCompensation(Enum):
            withComp = auto()
            withoutComp = auto()

        class ControlType(Enum):
            P = auto()
            PI = auto()
            PID = auto()

        class OutputType(Enum):
            disruption = auto()
            guided = auto()

        class Setup(NamedTuple):
            Kpr: float
            Ti: Optional[float]
            Td: Optional[float]

        class ZieglerNichols:
            # https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method
            pass

        class CHR:
            """
            Chien, Hrones und Reswick method
            """

            def __init__(
                self,
                k: float,
                Te: float,
                Tb: float,
                overshooting: bool,
                compensation: Control.PID.ControlCompensation,
                outputType: Control.PID.OutputType,
            ):
                """
                [summary]
                
                Parameters
                ----------
                Tb : float
                    Tb value
                Te : float
                    Te value
                k : float
                    May be Kps for "with compensation", Kis "without compensation"
                overshooting : bool
                    If we should allow overshooting
                compensation : Control.PID.ControlCompensation
                    What kind of compensation
                outputType : Control.PID.OutputType
                    The output type
                """
                self.Tb = Tb
                self.Te = Te
                self.k = k

                self.overshooting = overshooting
                self.compensation = compensation
                self.outputType = outputType

            def P(self):
                if self.compensation == Control.PID.ControlCompensation.withComp:
                    if self.overshooting:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.71 * (self.Tb / (self.k * self.Te)), None, None
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.71 * (self.Tb / (self.k * self.Te)), None, None
                            )

                    else:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.3 * (self.Tb / (self.k * self.Te)), None, None
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.3 * (self.Tb / (self.k * self.Te)), None, None
                            )

                elif self.compensation == Control.PID.ControlCompensation.withoutComp:
                    if self.overshooting:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.71 * (1 / (self.k * self.Te)), None, None
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.71 * (1 / (self.k * self.Te)), None, None
                            )

                    else:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.3 * (1 / (self.k * self.Te)), None, None
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.3 * (1 / (self.k * self.Te)), None, None
                            )

            def PI(self):
                if self.compensation == Control.PID.ControlCompensation.withComp:
                    if self.overshooting:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.71 * (self.Tb / (self.k * self.Te)),
                                2.3 * self.Te,
                                None,
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.59 * (self.Tb / (self.k * self.Te)), self.Tb, None
                            )

                    else:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.59 * (self.Tb / (self.k * self.Te)), 4 * self.Te, None
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.34 * (self.Tb / (self.k * self.Te)),
                                1.2 * self.Tb,
                                None,
                            )

                elif self.compensation == Control.PID.ControlCompensation.withoutComp:
                    if self.overshooting:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.71 * (1 / (self.k * self.Te)), 2.3 * self.Te, None
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.59 * (1 / (self.k * self.Te)), np.inf, None
                            )

                    else:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.59 * (1 / (self.k * self.Te)), 4 * self.Te, None
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.34 * (1 / (self.k * self.Te)), np.inf, None
                            )

            def PID(self):
                if self.compensation == Control.PID.ControlCompensation.withComp:
                    if self.overshooting:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                1.2 * (self.Tb / (self.k * self.Te)),
                                2.3 * self.Te,
                                0.42 * self.Te,
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.95 * (self.Tb / (self.k * self.Te)),
                                1.35 * self.Tb,
                                0.47 * self.Te,
                            )

                    else:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.95 * (self.Tb / (self.k * self.Te)),
                                2.4 * self.Te,
                                0.42 * self.Te,
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.59 * (self.Tb / (self.k * self.Te)),
                                self.Tb,
                                0.5 * self.Te,
                            )

                elif self.compensation == Control.PID.ControlCompensation.withoutComp:
                    if self.overshooting:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                1.2 * (1 / (self.k * self.Te)),
                                2 * self.Te,
                                0.42 * self.Te,
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.95 * (1 / (self.k * self.Te)), np.inf, 0.47 * self.Te
                            )

                    else:
                        if self.outputType == Control.PID.OutputType.disruption:
                            return Control.PID.Setup(
                                0.95 * (1 / (self.k * self.Te)),
                                2.4 * self.Te,
                                0.42 * self.Te,
                            )
                        elif self.outputType == Control.PID.OutputType.guided:
                            return Control.PID.Setup(
                                0.59 * (1 / (self.k * self.Te)), np.inf, 0.5 * self.Te
                            )
