# Type checking
from __future__ import annotations
from typing import (
    Union,
    TYPE_CHECKING,
    List,
    NoReturn,
    Dict,
    Optional,
    cast,
    Type,
    Tuple,
)

if TYPE_CHECKING:
    pass

# 3rd party imports
import numpy as np
import matplotlib.pyplot as plt


# Custom imports
from . import plotter
from .signal import Signal


class Generator:
    DEFAULT_SAMPLE_POINTS = 2 ** 20

    class GeneratedSignal:
        """A generated/synthetic signal

            The fEnd must be greater than f!
            
            Arguments:
                object {[type]} -- [description]
            
            Keyword Arguments:
                f {Union[int, float]} -- Frequency in [Hz] (default: {50})
                amplitude {Union[int, float]} -- Amplitude of the signal (default: {1.0})
                n {Union[int, float]} -- Number of periods (default: {2.0})
                phase_deg {Union[int, float]} -- Offset of the signal in [°] (default: {0.0})
                offset {Union[int, float]} -- DC offset in [V] (default: {0.0})
                n_sp {int} -- Number of samples, if None: Generator.DEFAULT_SAMPLE_POINTS (default: None)
                fEnd {Union[int, float]} -- End of the frequency in [Hz]. None->Same frequency across (default: {None})
            """

        def __init__(
            self,
            f: Union[int, float] = 50,
            amplitude: Union[int, float] = 1.0,
            n: Union[int, float] = 2.0,
            phase_deg: Union[int, float] = 0.0,
            offset: Union[int, float] = 0.0,
            n_sp: Optional[int] = None,
            fEnd: Optional[Union[int, float]] = None,
        ) -> None:
            if fEnd is not None and f > fEnd:
                raise ValueError(f"fEnd must be larger than f (f:{f} < fEnd:{fEnd})")

            # Create a new plotter
            self.plotter = plotter.Plotter()

            self._amplitude = amplitude
            self._n = n
            self._phase_deg = phase_deg
            self._offset = offset
            self._f = f
            self._n_sp = n_sp if n_sp is not None else Generator.DEFAULT_SAMPLE_POINTS
            self._fEnd = fEnd

            self.signalName = ""
            self._signal = Signal(self.signalName)
            self._signal.signalName = "Amplitude"
            self._signal.timeName = "Time [s]"

            self._helperSignals: Dict[str, Signal] = {}
            self._changedInputs = True

        @property
        def frequencyRange(self) -> np.array:
            if self._fEnd is not None:
                # Return an array of values
                return np.linspace(self.f, self.fEnd, num=self.n_sp)
            else:
                # Simply return the frequency
                return np.array([self.f])

        @property
        def fEnd(self) -> Optional[Union[float, int]]:
            """End frequency of the signal
            
            Returns:
                Optional[Union[float, int]] -- End frequency in [Hz]
            """
            return self._fEnd

        @fEnd.setter
        def fEnd(self, value: Optional[Union[float, int]]) -> None:
            """Set the ending frequency of the generator
            
            Arguments:
                value Optional[Union[float, int]] -- The new frequency in [Hz]
            """
            if value is not None and self.f > value:
                raise ValueError(
                    f"fEnd must be larger than f (f:{self.f} < fEnd:{value})"
                )

            self._fEnd = value
            self._changedInputs = True

        @property
        def amplitude(self) -> Union[float, int]:
            """
            Amplitude of the signal
            
            Returns:
                Union[float, int] -- Amplitude
            """
            return self._amplitude

        @amplitude.setter
        def amplitude(self, value):
            self._amplitude = value
            self._changedInputs = True

        @property
        def n(self):
            """
            Anzahl Perioden in der betrachteten Zeitspanne t_sp
            
            Returns:
                [type] -- [description]
            """
            return self._n

        @n.setter
        def n(self, n):
            self._n = n
            self._changedInputs = True

        @property
        def phase_deg(self) -> Union[float, int]:
            """
            Phase shift in [°]
            
            Returns:
                Union[float, int] -- Phase shift in [°]
            """
            return self._phase_deg

        @phase_deg.setter
        def phase_deg(self, value: Union[float, int]) -> None:
            self._phase_deg = value
            self._changedInputs = True

        @property
        def offset(self) -> Union[float, int]:
            """
            DC offset of the signal Signals
            
            Returns:
                Union[float, int] -- DC Offset in [V]
            """
            return self._offset

        @offset.setter
        def offset(self, value: Union[float, int]) -> None:
            """Update the DC offset
            
            Arguments:
                value {Union[float, int]} -- DC offset in [V]
            """
            self._offset = value
            self._changedInputs = True

        @property
        def f(self) -> Union[float, int]:
            """
            Frequency of the signal
            
            Returns:
                Union[float, int] -- Frequency of the signal in [Hz]
            """
            return self._f

        @f.setter
        def f(self, value: Union[float, int]) -> None:
            """Update the frequency of the signal

            Has to be smaller than fEnd!
            
            Arguments:
                value {Union[float, int]} -- The new frequency in [Hz]
            """
            if self.fEnd is not None and value > self.fEnd:
                raise ValueError(
                    f"fEnd must be larger than f (f:{value} < fEnd:{self.fEnd})"
                )

            self._f = value
            self._changedInputs = True

        @property
        def n_sp(self) -> Union[float, int]:
            """
            Number of sample points
            
            Returns:
                Union[float, int] -- Count of sample points
            """
            return self._n_sp

        @n_sp.setter
        def n_sp(self, value: int) -> None:
            """Update the number of sampling points
            
            Arguments:
                value {int} -- The new number of sampling points
            """
            if value <= 0:
                raise ValueError("You need to have one or more sampling points!")

            self._n_sp = value
            self._changedInputs = True

        @property
        def phase_rad(self) -> Union[float, int]:
            """Phase shift in [RAD] of the set shift in [DEG]
            
            Returns:
                Union[float, int] -- The phase shift in [RAD]
            """
            return self.phase_deg / 180 * np.pi

        @property
        def t_sp(self) -> np.ndarray:
            """time of the signal for all periods in [s]
            
            Returns:
                np.ndarray[float, int] -- The total signal time in [s]
            """
            return 1 / self.frequencyRange * self.n

        @property
        def w(self) -> np.ndarray:
            """1/s Angular frequency
            
            Returns:
                np.ndarray[float, int] -- Angular frequency
            """
            return 2 * np.pi * self.f

        @property
        def sp(self) -> np.ndarray:
            """Array with the size of the sampling points, spaced by 1
            
            Returns:
                np.ndarray[int] -- Array starting at zero to n_sp spaced by 1
            """
            return np.arange(0, self.n_sp, 1)

        @property
        def dt_sp(self) -> Union[float, int]:
            """Time between two sampling points
            
            Returns:
                Union[float, int] -- Time between two sampling points in [s]
            """
            return self.t_sp / (self.n_sp - 1)

        @property
        def t(self) -> np.ndarray:
            """Timevector
            
            Returns:
                np.ndarray -- Timevector
            """
            return self.dt_sp * self.sp

        @property
        def sampling_rate(self) -> np.ndarray:
            """Sampling rate over the frequency range
            
            Returns:
                np.ndarray -- Sampling rate
            """
            return self.n_sp / self.t_sp

        @property
        def signal(self) -> Signal:
            """Returns the signal of the signal generator
            
            Returns:
                Signal -- The signal of the generator
            """
            # Check if we changed the inputs
            if self._changedInputs:
                # Recalc
                self.calculate()

            # Return the calculated signal
            return self._signal

        def calculate(self) -> Signal:
            """
            Calculate the signal
            """
            pass

        def plot(self, update: bool = True) -> None:
            """Plot the signal of the generator
            
            Keyword Arguments:
                update {bool} -- If we should update the signal before plotting (default: {True})
            """
            if update:
                self.calculate()

            self.signal.plot()

        def __str__(self) -> str:
            """String representation of the generator
            
            Returns:
                str -- Description
            """
            return f"{self.signalName}: {self.signal}"

    @staticmethod
    def getSignals() -> List[str]:
        """
        Gets all the generated signal type names
        
        Returns:
            List[str] -- List of all the generator types
        """
        return [cls.__name__ for cls in Generator.GeneratedSignal.__subclasses__()]

    @staticmethod
    def signalFromName(name: str) -> Optional[Type[Generator.GeneratedSignal]]:
        """
        Generator init via a string
        
        Arguments:
            name {str} -- String name of the signal we should init
        
        Returns:
            Optional[Type[Generator.GeneratedSignal]] -- The signal generator reference
        """
        # Get all the signals
        signals = Generator.GeneratedSignal.__subclasses__()

        # Loop them
        for sig in signals:
            # Check if the name matches
            if sig.__name__ == name:
                # Return
                return sig

        # Nothing found
        return None

    @staticmethod
    def durationToFrequency(duration: Union[int, float]) -> Union[int, float]:
        """
        Convert the duration of a period to a frequency
        
        Parameters
        ----------
        duration : Union[int, float]
            The duration of the period
        
        Returns
        -------
        Union[int, float]
            The frequency
        """
        return 1 / duration

    @staticmethod
    def periodsInTime(
        time: Union[int, float], frequency: Union[int, float]
    ) -> Union[int, float]:
        """
        How many periods can fit inside the time?
        
        Parameters
        ----------
        time : Union[int, float]
            The time range we want to check
        frequency : Union[int, float]
            At which frequency our signal is
        
        Returns
        -------
        Union[int, float]
            How many periods will fit into our frequency in the given time
        """
        return time / (1 / frequency)

    @staticmethod
    def getFrequencyFor(
        time: Union[int, float], periods: Union[int, float], includeEnd: bool = True
    ) -> Union[int, float]:
        """
        Get the frequency for the given time where we need a certain amount of periods
        
        Parameters
        ----------
        time : Union[int, float]
            The time we would like
        periods : Union[int, float]
            How many periods
        includeEnd : bool
            If we whould include the end. Will add another period

        Returns
        -------
        Union[int, float]
            The frequency that fits
        """
        periods = periods + 1 if includeEnd else periods

        return 1 / (time / (periods))

    class DC(GeneratedSignal):
        """DC Offset generator
        """

        def __init__(self, *args, **kwargs) -> None:
            super().__init__(*args, **kwargs)
            self.signalName = "DC Offset"

        def calculate(self) -> Signal:
            """Calculate the signal. The offset is controlled by the offset parameter, the amplitude has no effect
            
            Returns:
                Signal -- The calculated signal
            """
            # Create a signal class
            self._signal.signal = (np.zeros(self.t.shape) + self.offset).astype(float)
            self._signal.time = self.t
            self._signal.name = self.signalName

            # Return it
            return self._signal

    class Sin(GeneratedSignal):
        """Sinus generator
        """

        def __init__(self, *args, **kwargs) -> None:
            super().__init__(*args, **kwargs)
            self.signalName = "Sin"

        def calculate(self) -> Signal:
            """Calculate the signal
            
            Returns:
                Signal -- The calculated signal
            """
            if self.fEnd is not None:
                # define desired logarithmic frequency sweep
                f_inst = np.logspace(np.log10(self.f), np.log10(self.fEnd), self.n_sp)
                phi = (
                    2 * np.pi * np.cumsum(f_inst) * self.dt_sp
                )  # integrate to get phase
                sinus = self.amplitude * np.sin(phi + self.phase_rad) + self.offset

            else:
                sinus = (
                    self.amplitude * np.sin(self.w * self.t + self.phase_rad)
                    + self.offset
                )

            # Create a signal class
            self._signal.signal = sinus
            self._signal.time = self.t
            self._signal.name = self.signalName

            # Return it
            return self._signal

    class Cos(GeneratedSignal):
        """Cosine generator
        """

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.signalName = "Cos"
            self.sinusHelper = Generator.Sin(*args, **kwargs)
            self.sinusHelper.offset = 0
            self.sinusHelper.phase_deg = 90

        def calculate(self) -> Signal:
            """Calculate the signal
            
            Returns:
                Signal -- The calculated signal
            """
            # Create a signal class
            signal = Signal(self.signalName, self.t, self.sinusHelper.signal.signal)
            signal.signalName = "Amplitude"

            self._signal.signal = signal.signal
            self._signal.time = self.t
            self._signal.name = self.signalName

            # Return it
            return self._signal

    class Square(GeneratedSignal):
        """Square signal generator
        """

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.signalName = "Square"
            self.sinusHelper = Generator.Sin(*args, **kwargs)
            self.sinusHelper.offset = 0

        def calculate(self) -> Signal:
            """Calculate the signal
            
            Returns:
                Signal -- The calculated signal
            """
            # Get the sinus
            sinus = self.sinusHelper.signal.signal

            # Calculate the square
            square = np.where(sinus >= 0, 1, -1) * self.amplitude + self.offset

            self._signal.signal = square.astype(float)
            self._signal.time = self.t
            self._signal.name = self.signalName

            # Add the helper signal
            self._signal.helperSignals["Sin"] = sinus

            # Return it
            return self._signal

    class Triangle(GeneratedSignal):
        """Triangle generator
        """

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.signalName = "Triangle"
            self.sinusHelper = Generator.Sin(*args, **kwargs)
            self.sinusHelper.offset = 0
            self.sinusHelper.amplitude = 1

        def calculate(self) -> Signal:
            """Calculate the signal
            
            Returns:
                Signal -- The calculated signal
            """
            sinus = self.sinusHelper.signal.signal

            arcsin = np.arcsin(sinus) * 2 / np.pi
            triangle = arcsin * self.amplitude + self.offset

            # Create a signal class
            self._signal.signal = triangle
            self._signal.time = self.t
            self._signal.name = self.signalName

            # Add the helper signal
            self._signal.helperSignals["Sin"] = sinus

            # Return it
            return self._signal

    class Sawtooth(GeneratedSignal):
        """Sawtooth generator

        Does not work over a frequency range!
        """

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.signalName = "Sawtooth"
            self.sinusHelper = Generator.Sin(*args, **kwargs)
            self.sinusHelper.offset = 0
            self.sinusHelper.amplitude = 1

        def calculate(self) -> Signal:
            """Calculate the signal
            
            Returns:
                Signal -- The calculated signal
            """
            if self.fEnd is not None:
                print(
                    "Warning: The amplitude is not consistent over the frequency range!"
                )

            sinus = self.sinusHelper.signal.signal
            arcsin = np.arcsin(sinus) * 2 / np.pi
            triangle = arcsin + self.offset
            differential = Generator.Sawtooth.differenzieren(self.t, triangle) * (
                0.25 / self.frequencyRange
            )
            sawtooth = triangle * differential * self.amplitude

            # Create a signal class
            self._signal.signal = sawtooth
            self._signal.time = self.t
            self._signal.name = self.signalName

            # Add the helper signal
            self._signal.helperSignals["Sin"] = sinus
            self._signal.helperSignals["Diff"] = differential
            self._signal.helperSignals["Triangle"] = triangle

            # Return it
            return self._signal

        @staticmethod
        def differenzieren(stellen, werte=None):
            y = werte
            x = stellen
            dy = []
            dy.append(y[0])  # 1. Eintrag entspricht dem Funktionswert
            for i in range(1, len(x)):
                dy.append((y[i] - y[i - 1]) / (x[i] - x[i - 1]))

            return np.transpose(dy)

    class PWM(GeneratedSignal):
        """PWM Generator
        
        Arguments:
            dutyCycle {float} -- Duty cycle of the signal (how much time is it on in %) (default: {0.75})
        """

        def __init__(self, dutyCycle: float = 0.75, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.signalName = "PWM"
            self.dutyCycle = dutyCycle
            self.sinusHelper = Generator.Sin(*args, **kwargs)
            self.sinusHelper.offset = 0
            self.sinusHelper.amplitude = 1

        def calculate(self) -> Signal:
            """Calculate the signal
            
            Returns:
                Signal -- The calculated signal
            """
            helperSin = self.sinusHelper.signal.signal
            helperTriangle = np.arcsin(helperSin) / np.pi + 0.5

            # Nullsignal
            PWMSignal = (
                np.where(helperTriangle < self.dutyCycle, self.amplitude, 0)
                + self.offset
            )

            # Create a signal class
            self._signal.signal = PWMSignal.astype(float)
            self._signal.time = self.t
            self._signal.name = self.signalName

            # Add the helper signals
            self._signal.helperSignals["Sin"] = helperSin
            self._signal.helperSignals["Triangle"] = helperTriangle

            # Return it
            return self._signal

    class Steps(GeneratedSignal):
        def __init__(self, includeEnd: bool = True, *args, **kwargs):
            """
            Step Generator. 
            
            fEnd Parameter currently does not work here
            
            Control the starting end end value using the amplitude and offset params.
            The time between the steps is determined by the frequency and the duration by the count of periods

            Parameters
            ----------
            includeEnd : bool
                If we whould include the end. Will add another period, by default True
            """
            super().__init__(*args, **kwargs)
            self.includeEnd = includeEnd

        def calculate(self) -> Signal:
            """
            Calculate the signal
            
            Returns
            -------
            Signal
                The calculated step signal
            """

            if self.fEnd is not None:
                raise Exception("fEnd is currently not implemented here!")

            if self.includeEnd is False:
                raise Exception("Currently not implemented")

            # Every period increase the value by x until we reach n periods (n_p)
            # x = (1 / n_p) * amplitude + offset
            # t = (1/ f) * n_p
            n = self.n

            if self.includeEnd:
                n += 1

            # Calculate the times
            timeValues = np.linspace(0, self.t_sp, n)
            timeValuesShifted = np.roll(timeValues, -1)
            timeValuesShifted[-1] = 1 / self.f * n

            # Calculate the step values
            stepValues = np.ones(timeValues.size) / (n - 1)
            stepValues = np.cumsum(stepValues) - stepValues[0]

            # Combine the step values
            c = np.empty((stepValues.size * 2,))
            c[0::2] = stepValues
            c[1::2] = stepValues

            c = (c * self.amplitude + self.offset).astype(float)

            # Combine the times
            t = np.empty((timeValues.size + timeValuesShifted.size, 1))
            t[0::2] = timeValues
            t[1::2] = timeValuesShifted
            t2 = np.transpose(t)

            # Create the signal
            sig = Signal("Steps", t2[0], c)

            return sig
