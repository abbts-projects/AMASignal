# Type checking
from __future__ import annotations
from typing import Union, TYPE_CHECKING, List, Dict, NamedTuple, Optional, cast

if TYPE_CHECKING:
    from .processing import Processing
    from .filter import Filter

# Other imports
from enum import Enum, auto
from collections import namedtuple
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import copy
import csv
from io import StringIO
import os

# custom imports
from AMASignal.exceptions import (
    UnsupportedOperation,
    SignalsDifferentShapes,
    UnsupportedSignal,
)


class Signal:
    class Types(Enum):
        line = auto()
        scatter = auto()
        std = auto()
        hline = auto()
        vline = auto()

    def __init__(
        self,
        name: str,
        time: Optional[np.ndarray] = None,
        signal: Optional[np.ndarray] = None,
        helperSignals: Optional[
            Union[
                np.ndarray,
                pd.Series,
                Signal,
                List[Union[np.ndarray, pd.Series, Signal]],
            ]
        ] = None,
        signalType: Signal.Types = Types.line,
    ):
        """
        Creates a Signal
        
        Parameters
        ----------
        name : str
            The name of the signal

        time : Optional[np.ndarray], optional
            The time points of the signal, by default None

        signal : Optional[np.ndarray], optional
            The y axis of the signal, by default None

        helperSignals : Optional[Union[np.ndarray, pd.Series, Signal, List[Union[np.ndarray, pd.Series, Signal]]]], optional
            Helper signal y values, by default None

        Raises
        ------
        UnsupportedSignal
            If the helper signal is not supported
        """
        # Dict of helper signals
        helperSignalsType = type(helperSignals)
        self.helperSignals: Dict[str, Signal] = {}

        if helperSignals is None:
            # Empty
            pass

        elif helperSignalsType is np.ndarray:
            # A list of values
            sig = Signal("Unnamed Signal", time, helperSignals)
            self.addSignalsAsHelpers(sig)

        elif helperSignalsType is pd.Series:
            # Cast
            helperSignals = cast(pd.Series, helperSignals)

            # Create a signal
            sig = Signal(helperSignals.name, time, helperSignals)
            self.addSignalsAsHelpers(sig)

        elif type(helperSignals) is list:
            # A List of signals
            helperSignals = cast(list, helperSignals)

            # Loop each signal
            for helper in helperSignals:
                # Check type
                helperType = type(helper)
                if helperType is np.ndarray:
                    # Create a name / Signal
                    sig = Signal("Unnamed Signal", time, helper)
                    self.addSignalsAsHelpers(sig)

                elif helperType is pd.Series:
                    # Cast
                    helperSignals = cast(pd.Series, helperSignals)

                    # Create a signal
                    sig = Signal(helperSignals.name, time, helperSignals)
                    self.addSignalsAsHelpers(sig)

                elif helperType is Signal:
                    # Add the signal
                    self.addSignalsAsHelpers(helper)
                else:
                    # Hmmm
                    raise UnsupportedSignal(f"Unknown Type of signal: {helperType}")

        else:
            # Nope
            raise UnsupportedSignal(f"Unknown Type of signal: {helperSignalsType}")

        # Important data
        if type(signal) is not np.ndarray:
            signal = np.array(signal)

        if type(time) is not np.ndarray:
            time = np.array(time)

        self.signal: np.ndarray = signal  # y
        self.time: np.ndarray = time  # x

        # Name of the signal
        self.name: str = name

        # If this signal is actually a list of points
        self.type = signalType

        # Axis titles for the plot/csv
        self.timeName: str = ""
        self.signalName: str = ""

    def __add__(self, other: Signal) -> Signal:
        """
        Add one signal to another signal
        
        Parameters
        ----------
        other : Signal
            The other signal to be added
        
        Returns
        -------
        Signal
            The combined signal
        
        Raises
        ------
        UnsupportedOperation
            [description]
        SignalsDifferentShapes
            [description]
        """
        # Check if the other one is also of out class
        if not isinstance(other, (Signal, int, float)):
            raise UnsupportedOperation(f"Can't subtract a {other} from a signal!")

        if isinstance(other, Signal):
            if len(self.signal) != len(other.signal):
                raise SignalsDifferentShapes(
                    "The two signals are not of the same shape!"
                )

            # Combine
            newSignal = self.signal + other.signal
            otherName = other.name

        else:
            newSignal = self.signal + other
            otherName = str(other)

        # Return the new signal
        sig = Signal(f"({self.name} + {otherName})", self.time, newSignal)
        sig.timeName = self.timeName
        sig.signalName = self.signalName

        return sig

    def __sub__(self, other: Signal) -> Signal:
        """
        Subtract a signal
        
        Parameters
        ----------
        other : Signal
            The other signal that should be subtracted from our signal
        
        Returns
        -------
        Signal
            The new signal after subtraction
        
        Raises
        ------
        UnsupportedOperation
            If the operation is not supported
        SignalsDifferentShapes
            If the signals do not have the same shapes
        """
        # Check if the other one is also of out class
        if not isinstance(other, (Signal, int, float)):
            raise UnsupportedOperation(f"Can't subtract a {other} from a signal!")

        if isinstance(other, Signal):
            if len(self.signal) != len(other.signal):
                raise SignalsDifferentShapes(
                    "The two signals are not of the same shape!"
                )

            # Combine
            newSignal = self.signal - other.signal
            otherName = other.name

        else:
            newSignal = self.signal - other
            otherName = str(other)

        # Return the new signal
        sig = Signal(f"({self.name} - {otherName})", self.time, newSignal)
        sig.timeName = self.timeName
        sig.signalName = self.signalName

        return sig

    def __mul__(self, other: Signal) -> Signal:
        # Check if the other one is also of out class
        # Check if the other one is also of out class
        if not isinstance(other, (Signal, int, float)):
            raise UnsupportedOperation(f"Can't subtract a {other} from a signal!")

        if isinstance(other, Signal):
            if len(self.signal) != len(other.signal):
                raise SignalsDifferentShapes(
                    "The two signals are not of the same shape!"
                )

            # Combine
            newSignal = self.signal * other.signal
            otherName = other.name

        else:
            newSignal = self.signal * other
            otherName = str(other)

        # Return the new signal
        sig = Signal(f"({self.name} * {otherName})", self.time, newSignal)
        sig.timeName = self.timeName
        sig.signalName = self.signalName

        return sig

    def __truediv__(self, other: Signal) -> Signal:
        # Check if the other one is also of out class
        if not isinstance(other, (Signal, int, float)):
            raise UnsupportedOperation(f"Can't subtract a {other} from a signal!")

        if isinstance(other, Signal):
            if len(self.signal) != len(other.signal):
                raise SignalsDifferentShapes(
                    "The two signals are not of the same shape!"
                )

            # Combine
            newSignal = self.signal / other.signal
            otherName = other.name

        else:
            newSignal = self.signal / other
            otherName = str(other)

        # Return the new signal
        sig = Signal(f"({self.name} / {otherName})", self.time, newSignal)
        sig.timeName = self.timeName
        sig.signalName = self.signalName

        return sig

    def __rtruediv__(self, other: Signal) -> Signal:
        # Combine
        newSignal = other / self.signal

        # Return the new signal
        sig = Signal(f"({other}/{self.name})", self.time, newSignal)
        sig.timeName = self.timeName
        sig.signalName = self.signalName

        return sig

    def __str__(self):
        return f"""name: {self.name}
    time: {self.time}
    signal: {self.signal}"""

    @property
    def dbSignal(self):
        sig = copy.deepcopy(self)
        if np.iscomplex(sig.signal[0]):
            sig.signal = np.abs(sig.signal)

        sig.signal = 20 * np.log10(sig.signal)

        # Return the copy of the signal
        return sig

    @property
    def angleSignal(self):
        sig = copy.deepcopy(self)
        sig.signal = np.angle(self.signal, deg=True)

        # Return the copy of the signal
        return sig

    @property
    def startFinishTimes(self):
        """
        Get the starting end ending time of the signal
        """
        return np.array([self.time[0], self.time[-1]])

    def timedelta(self, returnMean: bool = False) -> float:
        """
        Get the time delta between measurements

        This only returns correct values, if they are spaced evenly!
        
        
        Parameters
        ----------
        returnMean : bool, optional
            If we should return the mean instead of the diff, by default False
        
        Returns
        -------
        float
            The time delta between two points in [s]
        
        Raises
        ------
        Exception
            If we can't return the mean and the time differences are not even
        """
        # Get the signal and the shifted signal
        shiftedTime = np.roll(self.time, -1)[:-1]
        time = self.time[:-1]
        delta = shiftedTime - time

        # Check if there is no change to the next value
        if returnMean:
            return np.mean(delta)

        elif min(delta) == max(delta):
            return delta[0]

        else:
            raise Exception("The time between the measurements is not constant")

    def plot(self, show=True, axes=None, showHelpers=True):
        if axes is None:
            # Create a new axis
            _, axes = plt.subplots(1, 1, subplot_kw=dict(projection="GeneralAxes"))

        # Plot the main signal
        axes.plotMainSignal(self)

        if showHelpers:
            # Helper signals
            axes.plotHelperSignals(self.helperSignals)

        # Check if we should show it
        if show:
            plt.show()

    def bodePlot(self):
        _, axes = plt.subplots(1, 1, subplot_kw=dict(projection="GeneralAxes"))
        axes.plotMainSignal(self, y=self.dbSignal)
        axes.plotHelperSignals(self.helperSignals)

        plt.show()

    def phaseResponse(self):
        _, axes = plt.subplots(1, 1, subplot_kw=dict(projection="GeneralAxes"))

        axes.plotMainSignal(self, y=self.angleSignal)
        axes.plotHelperSignals(self.helperSignals)

        plt.show()

    def process(
        self, *args: Processing.Signalprocessor, compact: bool = True
    ) -> Union[List[Signal], Signal]:
        """
        Process multiple signals
        
        Parameters
        ----------
        *args : List[Processing.Signalprocessor]
            A list of all the processors

        compact : bool, optional
            If we should return a single value if one processor is provided or a list, by default True
        
        Returns
        -------
        Union[List[Signal], Signal]
            A list or a single Signal depending on the inputs
        """
        signals: List[Signal] = []

        # Loop each one
        for processor in args:
            # Process the signal
            sig = processor.processSignal(self)

            if isinstance(sig, Signal):
                # Add a sigle signal
                sig = cast(Signal, sig)
                signals.append(sig)

            elif type(sig) is list:
                # Add a list of signals
                sig = cast(List[Signal], sig)
                signals.extend(sig)

        # Check how to return
        if len(signals) == 1 and compact:
            # Return the single signal
            return signals[0]

        else:
            # Return the list of signals
            return signals

    def applyFilter(
        self, *args: Filter.SignalFilter, compact: bool = True, serial: bool = True
    ) -> Union[List[Signal], Signal]:
        """
        Apply filters to a signal
        
        Parameters
        ----------
        *args : Filter.SignalFilter
            A list of all the filters to apply

        compact : bool, optional
            If we should return a single value if one processor is provided or a list, by default True
        
        serial: bool, optional
            If each signal should be filtered with the previous filtered data

        Returns
        -------
        Union[List[Signal], Signal]
            A list or a single Signal depending on the inputs
        """
        signals: List[Signal] = []

        # Loop each one
        for sigFilter in args:
            # Check how to filter
            if serial and len(signals) > 0:
                # Serial
                toBeFilteredSignal = signals[0]
            else:
                # Singular
                toBeFilteredSignal = self

            # Process the signal
            sig = sigFilter.filterSignal(toBeFilteredSignal)

            # Check how to add it
            if serial:
                signals = [sig]
            else:
                signals.append(sig)

        # Check how to return
        if len(signals) == 1 and compact:
            # Return the single signal
            return signals[0]

        else:
            # Return the list of signals
            return signals

    def addSignalsAsHelpers(self, *args: Union[List[Signal], Signal]) -> None:
        # Rename
        signals = args

        # Check the type
        if type(signals) is Signal:
            # Just a single signal
            signals = [signals]

        elif signals is None:
            # Empty signal
            return

        # Set the new type
        signals = cast(List[Signal], signals)

        # Loop each signal
        for signal in signals:
            # Check if this is a list
            if type(signal) in (list, tuple):
                # Okay, add it to ourselfs
                self.addSignalsAsHelpers(*signal)
                continue

            elif signal is None:
                # Nothing here
                continue

            # Check if this signal exists
            count = 1

            # Recast
            signal = cast(Signal, signal)
            name = signal.name

            # Loop till we find an available name
            while True:
                # Check if the name exists
                if name in self.helperSignals:
                    count += 1
                    name = f"{signal.name} ({count})"
                    continue

                # Found a free name
                signal.name = name
                self.helperSignals[name] = signal
                break

    def getIntersections(self, sig: Signal) -> Optional[Signal]:
        """
        Get the intersections of the current signal and sig
        
        Parameters
        ----------
        sig : Signal
            The other signal that intersects the current one
        
        Returns
        -------
        Optional[Signal]
            Will return a Signal if we have intersections, otherwise None
        """
        # Check we are a simple v or h line
        if self.type is Signal.Types.hline or self.type is Signal.Types.vline:
            sigA_0 = np.full(len(self.time), self.signal)
            sigA_1 = sigA_0
        else:
            # Shift the points, so on the same index, one of them is shifter by 1
            sigA_0 = self.signal[:-1]
            sigA_1 = np.roll(self.signal, -1)[:-1]

        timeA_0 = self.time[:-1]
        timeA_1 = np.roll(self.time, -1)[:-1]

        # Check if this a simple v or h line
        if sig.type is Signal.Types.hline or sig.type is Signal.Types.vline:
            sigB_0 = np.full(len(sig.time), sig.signal)
            sigB_1 = sigB_0
        else:
            sigB_0 = sig.signal[:-1]
            sigB_1 = np.roll(sig.signal, -1)[:-1]

        timeB_0 = sig.time[:-1]
        timeB_1 = np.roll(sig.time, -1)[:-1]

        # Check which one is shorter
        loopCount = min(len(timeA_0), len(timeB_0))
        points = []

        # Loop all the points
        for i in range(loopCount):
            # Generate all the points
            A1 = (timeA_0[i], sigA_0[i])
            A2 = (timeA_1[i], sigA_1[i])
            B1 = (timeB_0[i], sigB_0[i])
            B2 = (timeB_1[i], sigB_1[i])

            # Check if there is an intersection
            x, y = Signal.__getIntersection(A1, A2, B1, B2)

            # Check if we have an intersection
            if not np.isinf(x) and not np.isinf(y):
                # Found a point
                points.append((x, y))

        # Check if we have any intersections
        if len(points) > 0:
            # Transpose the points into a vertical alignment
            points = np.transpose(points)

            # Create the signal
            sig = Signal("Intersections", points[0], points[1])
            sig.type = Signal.Types.scatter

            # Return the signal
            return sig

        else:
            # Nothing
            return None

    @staticmethod
    def __getIntersection(a1, a2, b1, b2, limitX=True, limitY=True):
        """ 
        Returns the point of intersection of the lines passing through a2,a1 and b2,b1.
        a1: [x, y] a point on the first line
        a2: [x, y] another point on the first line
        b1: [x, y] a point on the second line
        b2: [x, y] another point on the second line

        limitX: If we should limit the intersection to the X of the inputs
        limitY: If we should limit the intersection to the Y of the inputs

        https://stackoverflow.com/questions/3252194/numpy-and-line-intersections
        """
        s = np.vstack([a1, a2, b1, b2])  # s for stacked
        h = np.hstack((s, np.ones((4, 1))))  # h for homogeneous
        l1 = np.cross(h[0], h[1])  # get first line
        l2 = np.cross(h[2], h[3])  # get second line
        x, y, z = np.cross(l1, l2)  # point of intersection

        if z == 0:  # lines are parallel
            return (float("inf"), float("inf"))

        if limitX:
            # Limit the x range to the inputs
            xMin = min(a1[0], a2[0], b1[0], b2[0])
            xMax = max(a1[0], a2[0], b1[0], b2[0])
            if x / z >= xMax or x / z <= xMin:
                return (float("inf"), float("inf"))

        if limitY:
            # Limit the x range to the inputs
            yMin = min(a1[1], a2[1], b1[1], b2[1])
            yMax = max(a1[1], a2[1], b1[1], b2[1])
            if y / z >= yMax or y / z <= yMin:
                return (float("inf"), float("inf"))

        # Found some points
        return (x / z, y / z)

    def copy(self) -> Signal:
        """
        Copy a signal

        Does not copy the helper signals!
        
        Returns
        -------
        Signal
            A new copy of the current signal
        """
        return Signal(self.name, self.time.copy(), self.signal.copy(), signalType=self.type)

    class IO:
        class CSVDialect(NamedTuple):
            """
            CSV Dialect
            """

            sep: str
            header: Union[List[str], None, bool] = None
            quoting: int = csv.QUOTE_MINIMAL
            encoding: str = "UTF-8"

        CSVTabDialect = CSVDialect(sep="\t", header=True)
        CSVCommaDialect = CSVDialect(sep=",", header=True)
        CSVCommaDialectWOHeader = CSVDialect(sep=",")

        @staticmethod
        def readCSV(
            path: Union[str, StringIO],
            timeAxis: Union[str, int],
            dialect: CSVDialect = CSVCommaDialect,
            converters: Optional[dict] = None,
            compact: bool = True,
        ) -> Union[List[Signal], Signal]:
            """
            Reads a CSV file.
            
            Parameters
            ----------
            path : Union[str, StringIO]
                The path to the string or StringIO object

            timeAxis : Union[str, int]
                The name of the time axis or index

            dialect : Optional[CSVDialect], optional
                The dilatect of the CSV, by default CSVCommaDialect

            converters : Optional[dict], optional
                A dict with converters for np.genfromtxt, by default None
            
            compact : bool
                If the output should be compacted if possible. If only one signal
                is found in the csv, it will only return the signal and not an 
                entire list

            Returns
            -------
            List[Signal]
                A list of signals with a common time axis
            
            Raises
            ------
            ValueError
                If the time axis was not found in the data
            """
            # Get the correct path
            path = os.path.expanduser(path)

            # Read the file
            with open(path) as fh:
                dat = fh.readlines()

                dat = [line for line in dat if "#" not in line]

            # Read the csv
            data = np.genfromtxt(
                StringIO("\n".join(dat)),
                names=dialect.header,
                delimiter=dialect.sep,
                converters=converters,
                deletechars="",
                encoding=dialect.encoding,
                comments="#",
            )

            # Transpose the data
            data = data.transpose()

            # Get the header names
            if dialect.header and type(timeAxis) is not int:
                header = list(data.dtype.names)
            else:
                header = list(range(data.shape[0]))

            if type(timeAxis) is str and timeAxis not in header:
                raise ValueError(
                    f"The time axis '{timeAxis}' is not in the header: {header}"
                )

            # Create a signal for each column
            if type(timeAxis) is str:
                # Try to remove the value as a str
                header.remove(timeAxis)

            elif type(timeAxis) is int:
                # Remove by index
                timeAxis = cast(int, timeAxis)
                del header[timeAxis]

            else:
                raise Exception(f"Unknown type of timeAxis: {type(timeAxis)}")

            timeData = data[timeAxis]
            signals = []

            # Check if we have a string path
            filename = ""
            if type(path) is str:
                # recast
                path = cast(str, path)
                filename = os.path.basename(path)

            # Loop each header
            for head in header:
                # Get the name
                if type(head) is int:
                    # Create a nice header
                    headname = f"{filename} ({head})"
                else:
                    headname = head

                # Create the signal
                signal = Signal(head)
                signal.signal = data[head]
                signal.time = timeData
                signal.name = headname

                # add it to the list of signals
                signals.append(signal)

            # Check if we should be compact
            if compact and len(signals) == 1:
                # Return a sigle signal
                return signals[0]

            else:
                # Return the list of signals
                return signals

        @staticmethod
        def writeCSV(signal: Signal, path, dialect=CSVCommaDialect, floatFormat="%.9f"):
            timeName = "Time" if len(signal.timeName) == 0 else signal.timeName
            sigName = signal.name if len(signal.signalName) == 0 else signal.signalName

            data = {timeName: signal.time, sigName: signal.signal}

            for i, helper in enumerate(signal.helperSignals):
                if isinstance(helper, str):
                    sig = signal.helperSignals.get(helper, None)
                    if isinstance(sig, Signal):
                        data[sig.name] = sig.signal
                    else:
                        raise Exception("Unknown data!")

                elif isinstance(helper, dict):
                    data.update(helper)

                else:
                    data[str(i)] = helper

            try:
                df = pd.DataFrame(data)
                df.to_csv(
                    path,
                    sep=dialect.sep,
                    header=dialect.header,
                    quoting=dialect.quoting,
                    index=False,
                    float_format=floatFormat,
                    encoding=dialect.encoding,
                )

            except ValueError as e:
                print(f"Could not save data... ({e})")
