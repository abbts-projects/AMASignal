"""
General signal filters
"""
# 3rd party imports
from typing import Union
import numpy as np
import scipy.signal as scipySignal

# Custom imports
from .signal import Signal


class Filter:
    """
    A container for all filters
    """

    class SignalFilter:
        """
        A Signal filter to be subclassed
        """

        def __init__(self):
            pass

        def filterSignal(self, signal: Signal) -> Signal:
            """
            Filter a signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be filtered
            
            Returns
            -------
            Signal
                The filtered signal
            """
            pass

    class SavitzkyGolayFilter(SignalFilter):
        """
        Savitzky Golay Filter
        
        Parameters
        ----------
        windowSize : int, optional
            Window size, must be odd, by default 21

        polyorder : int, optional
            Polyorder, must be smaller than the window size, by default 3

        **kwargs : dict
            Any other options for scipy.signal.savgol_filter
        """

        def __init__(self, windowSize: int = 21, polyorder: int = 3, **kwargs):
            self.windowSize = windowSize
            self.polyorder = polyorder
            self.kwargs = kwargs

        def filterSignal(self, signal: Signal) -> Signal:
            """
            Apply the filter
            
            Parameters
            ----------
            signal : Signal
                The signal to be filtered
            
            Returns
            -------
            Signal
                The filtered signal
            """
            # Filter
            filtered = scipySignal.savgol_filter(
                signal.signal,
                window_length=self.windowSize,
                polyorder=self.polyorder,
                **self.kwargs
            )

            # Create a new signal
            sig = Signal("Savitzky Golay Filtered", signal.time, filtered)
            return sig

    class Convolution(SignalFilter):
        def filterSignal(self, signal: Signal) -> Signal:
            pass

    class Lowpass(SignalFilter):
        def __init__(self, cutoff: float, f: float, order: int = 5):
            """
            [summary]
            
            Parameters
            ----------
            cutoff : float
                At what frequency should the cutoff be in [Hz]
            
            f : float
                Sample rate in [Hz]
            
            order : int
                What order, by default 5
            
            """
            super().__init__()

            self.cutoff = cutoff
            self.f = f
            self.order = order

        def filterSignal(self, signal: Signal) -> Signal:
            """
            Apply a lowpass filter
            
            Parameters
            ----------
            signal : Signal
                The signal to be filtered

            Returns
            -------
            Signal
                The filtered signal
            """
            super().filterSignal(signal)

            # Filter
            newSignal = self.__butter_lowpass_filter(
                signal.signal, self.cutoff, self.f, self.order
            )

            # Return the signal
            return Signal("Lowpass " + signal.name, signal.time, newSignal)

        """
        Taken from https://stackoverflow.com/questions/25191620/creating-lowpass-filter-in-scipy-understanding-methods-and-units
        """

        def __butter_lowpass(self, cutoff, fs, order=5):
            nyq = 0.5 * fs
            normal_cutoff = cutoff / nyq
            b, a = scipySignal.butter(order, normal_cutoff, btype="low", analog=False)
            return b, a

        def __butter_lowpass_filter(self, data, cutoff, fs, order=5):
            b, a = self.__butter_lowpass(cutoff, fs, order=order)
            y = scipySignal.lfilter(b, a, data)
            return y
