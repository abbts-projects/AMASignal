# -*- coding: utf-8 -*-
# Type checking
from __future__ import annotations
from typing import Union, TYPE_CHECKING, List, cast, Any, Optional

if TYPE_CHECKING:
    pass

# 3rd party imports
import numpy as np

# Custom imports
from .generator import Generator
from .exceptions import UnsupportedSignal, UnsupportedOperation
from .signal import Signal


class Physical:
    siPrefix = {
        "y": 1e-24,  # yocto
        "z": 1e-21,  # zepto
        "a": 1e-18,  # atto
        "f": 1e-15,  # femto
        "p": 1e-12,  # pico
        "n": 1e-9,  # nano
        "u": 1e-6,  # micro
        "m": 1e-3,  # mili
        "c": 1e-2,  # centi
        "d": 1e-1,  # deci
        "k": 1e3,  # kilo
        "M": 1e6,  # mega
        "G": 1e9,  # giga
        "T": 1e12,  # tera
        "P": 1e15,  # peta
        "E": 1e18,  # exa
        "Z": 1e21,  # zetta
        "Y": 1e24,  # yotta
    }

    class PhysicalComponent:
        """
        Physical component

        Raises:
            UnsupportedSignal: When trying to use an unsupported signal
            UnsupportedOperation: When trying to do an unsupported operation
            Exception: General Error
        """

        def __init__(
            self,
            generator: Generator.GeneratedSignal,
            siPrefix: str = "",
            name: str = None,
        ) -> None:
            """
            Creates a physical component
            
            Arguments:
                generator {Generator.GeneratedSignal} -- The signal generator
            
            Keyword Arguments:
                siPrefix {str} -- SI Prefix (default: {""})
                name {str} -- The name of the signal (default: {None})
            
            Raises:
                UnsupportedSignal: When trying to use an unsupported signal
            """
            # Check if this is a supported signal
            if not isinstance(generator, Generator.Sin) and not isinstance(
                generator, Generator.Cos
            ):
                # Nope
                raise UnsupportedSignal(
                    f"{generator.signalName} is not supported as a signal!"
                )

            try:
                # Check if we have the component defined
                self.component: str

            except:
                # No, so let's do it
                self.component = "Component"

            self.name = name if name is not None else self.component
            self.siPrefix = siPrefix
            self.ZHistory = Signal(self.name)
            self.ZHistory.signalName = "Resistance Z [Ohm]"
            self.ZHistory.timeName = "Frequency [Hz]"

            # Set the new generator
            self._generator = generator

        def __add__(self, other: Physical.PhysicalComponent) -> Signal:
            """Add two physical components
            
            Arguments:
                other {Physical.PhysicalComponent} -- The other physical component to add
            
            Raises:
                UnsupportedOperation: When trying to do an unsupported operation
            
            Returns:
                Signal -- A new Signal where both signals are combined into one via addition
            """
            # Check if the other one is also of out class
            if not isinstance(other, Physical.PhysicalComponent):
                raise UnsupportedOperation(
                    f"Can't add a {type(other)} to a physical component!"
                )

            # Combine and return
            return self.ZHistory + other.ZHistory

        def __sub__(self, other: Physical.PhysicalComponent) -> Signal:
            """Subtract two physical components
            
            Arguments:
                other {Physical.PhysicalComponent} -- The other physical component to subtract
            
            Raises:
                UnsupportedOperation: When trying to do an unsupported operation
            
            Returns:
                Signal -- A new Signal where both signals are combined into one via subtraction
            """
            # Check if the other one is also of out class
            if not isinstance(other, Physical.PhysicalComponent):
                raise UnsupportedOperation(
                    f"Can't subtract a {type(other)} to a physical component!"
                )

            # Combine and return
            return self.ZHistory - other.ZHistory

        def __mul__(self, other: Physical.PhysicalComponent) -> Signal:
            """Multiply two physical components
            
            Arguments:
                other {Physical.PhysicalComponent} -- The other physical component to multiply
            
            Raises:
                UnsupportedOperation: When trying to do an unsupported operation
            
            Returns:
                Signal -- A new Signal where both signals are combined into one via multiplication
            """
            # Check if the other one is also of out class
            if not isinstance(other, Physical.PhysicalComponent):
                raise UnsupportedOperation(
                    f"Can't multiply a {type(other)} with a physical component!"
                )

            # Combine and return
            return self.ZHistory * other.ZHistory

        def __truediv__(self, other: Physical.PhysicalComponent) -> Signal:
            """Divide two physical components
            
            Arguments:
                other {Physical.PhysicalComponent} -- The other physical component to divide by
            
            Raises:
                UnsupportedOperation: When trying to do an unsupported operation
            
            Returns:
                Signal -- A new Signal where both signals are combined into one via division
            """
            # Check if the other one is also of out class
            if not isinstance(other, Physical.PhysicalComponent):
                raise UnsupportedOperation(
                    f"Can't divide a physical component with {type(other)}!"
                )

            # Combine and return
            return self.ZHistory / other.ZHistory

        def __rtruediv__(self, other: Any) -> Signal:
            """
            Divide something by the physical component
            
            Arguments:
                other {Any} -- What we should divide with the physical component
            
            Returns:
                Signal -- A new Signal where both signals are combined into one via division
            """
            return other / self.ZHistory

        @property
        def generator(self) -> Generator.GeneratedSignal:
            """Get the generator property
            
            Returns:
                Generator.GeneratedSignal -- The current generator
            """
            return self._generator

        @generator.setter
        def generator(self, generator: Generator.GeneratedSignal) -> None:
            """Set the new generator. Will recalculate the signal
            
            Arguments:
                generator {Generator.GeneratedSignal} -- The new generator
            """
            # Update
            self._generator = generator

            # Update the history
            self.recalc()

        @property
        def Z(self) -> Optional[Signal]:
            """Z Value
            
            Raises:
                Exception: When it has not been implemented into the subclass
            """
            raise Exception("Not implemented")

        def recalc(self) -> None:
            """
            Recalculate the Z history
            """
            if self.Z is not None:
                self.ZHistory.signal = self.Z.signal
                self.ZHistory.time = self.Z.time

    class Resistor(PhysicalComponent):
        def __init__(self, R: Union[int, float], *args, **kwargs) -> None:
            """Creates a Resistor. Calculates the Z value once created
            
            Arguments:
                r {Union[int, float]} -- Resistance in [Ohm]
            """
            self._R = R * Physical.siPrefix.get(kwargs.get("siPrefix", ""), 1)
            self.component = "Resistor"
            super().__init__(*args, **kwargs)
            self.ZHistory.name = self.name

            # Update the history
            self.recalc()

        @property
        def R(self) -> Union[int, float]:
            """Returns the resistance
            
            Returns:
                Union[int, float] -- The resistance in [Ohm]
            """
            return self._R

        @property
        def Z(self) -> Signal:
            """Calculate the Z value for the generator frequency range
            
            Returns:
                Signal -- A signal with the Z value
            """
            Z = np.full(len(self._generator.frequencyRange), self._R)

            return Signal("Resistor Z", self._generator.frequencyRange, Z)

    class Inductor(PhysicalComponent):
        def __init__(self, L: Union[int, float], *args, **kwargs) -> None:
            """Inductor. Calculates the Z value once created
            
            Arguments:
                L {Union[int, float]} -- Inductance in [H]
            """
            self._L = L * Physical.siPrefix.get(kwargs.get("siPrefix", ""), 1)
            self.component = "Inductor"
            super().__init__(*args, **kwargs)
            self.ZHistory.name = self.name

            # Update the history
            self.recalc()

        @property
        def L(self) -> Union[int, float]:
            """Returns the Inductance
            
            Returns:
                Union[int, float] -- Inductance in [H]
            """
            return self._L

        @property
        def Z(self) -> Signal:
            """Returns the Z over the frequency range of the generator
            
            Returns:
                Signal -- A signal with the Z value
            """
            Z = 1j * 2 * np.pi * self._generator.frequencyRange * self._L

            return Signal("Inductor Z", self._generator.frequencyRange, Z)

    class Capacitor(PhysicalComponent):
        def __init__(self, C: Union[int, float], *args, **kwargs) -> None:
            """Capacitor. Calculates the Z value once created
            
            Arguments:
                C {Union[int, float]} -- Capacitance in [F]
            """
            self._C = C * Physical.siPrefix.get(kwargs.get("siPrefix", ""), 1)
            self.component = "Capacitor"
            super().__init__(*args, **kwargs)
            self.ZHistory.name = self.name

            # Update the history
            self.recalc()

        @property
        def C(self) -> Union[int, float]:
            """Returns the Capacitance
            
            Returns:
                Union[int, float] -- Capacitance in [F]
            """
            return self._C

        @property
        def Z(self) -> Signal:
            """Returns the Z over the frequency range of the generator
            
            Returns:
                Signal -- A signal with the Z value
            """
            Z = 1 / (1j * 2 * np.pi * self._generator.frequencyRange * self._C)

            return Signal("Capacitor Z", self._generator.frequencyRange, Z)
