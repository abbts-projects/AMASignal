# Standard imports
from typing import Optional, List, Union, NamedTuple, Tuple, Callable, Dict
from enum import Enum, auto
import threading
from queue import Queue

# 3rd Party imports
import numpy as np
import dwf

# Custom imports
from AMASignal.signal import Signal


class WaveForms:
    """
    https://github.com/amuramatsu/dwf
    https://reference.digilentinc.com/_media/waveforms_sdk_reference_manual.pdf
    """

    MAX_SAMPLE_POINTS = 2 ** 16

    class OutputNotDefined(Exception): pass
    class DeviceNotFound(Exception): pass
    class DWFError(Exception): pass

    class Device(NamedTuple):
        """
        Placeholder for a Analog dicscovery device
        """

        device: dwf.DwfDevice
        dwf: Optional[dwf.Dwf]
        idx: int
        name: str
        serial: str
        isOpened: bool

    def __init__(self, device: Optional[Device] = None):
        # Init the outputs
        self.analog: Optional[AnalogWaveforms] = None
        self.digital: Optional[DigitalWaveforms] = None

        # Set the device index, updates everything
        self.device = device

    def getVersion(self):
        return dwf.FDwfGetVersion()

    @property
    def device(self) -> Optional[Device]:
        return self._device

    @device.setter
    def device(self, x: Optional[Device]):
        self._device = x

        # Check if it's none
        if x is None:
            # Reset
            if self.analog is not None:
                self.analog.close()
                self.analog = None

            if self.digital is not None:
                self.digital.close()
                self.digital = None

        else:
            # Analog setup
            if self.analog is None:
                self.analog = AnalogWaveforms(x)
            else:
                self.analog.close()
                self.analog.deviceIndex = x

            # Digital setup
            if self.digital is None:
                # self.digital = DigitalWaveforms(x.idx)
                pass
            else:
                self.digital.close()
                self.digital.deviceIndex = x

    def listAllDevices(self) -> List[Device]:
        """
        List all the devices connected
        
        Returns
        -------
        List[WaveForms.Device]
            A list of all connected devices
        """
        # Get all devices
        devices = dwf.DwfEnumeration()
        devList = []

        # Loop each device
        for device in devices:
            # Get some info
            devList.append(
                WaveForms.Device(
                    device=device,
                    dwf=None,
                    idx=device.idxDevice,
                    name=device.deviceName(),
                    serial=device.SN(),
                    isOpened=device.isOpened(),
                )
            )

        # Return the list
        return devList

    def getDeviceBySN(
        self,
        serial: str,
        devices: Optional[List[Device]] = None,
        setAsCurrent: bool = True,
    ) -> Optional[Device]:
        """
        Get a device by it's Serial number
        
        Parameters
        ----------
        serial : str
            The Serial number of the device, without the SN: infront

        devices : Optional[List[Device]], optional
            A custom list of connected devices, by default None

        setAsCurrent : bool
            If we should set it as the current device or not

        Returns
        -------
        Optional[Device]
            A single device of None if none are found
        """
        # Check if we have a device list
        if devices is None:
            devices = self.listAllDevices()

        # Add SN
        serial = "SN:" + serial

        # Filter
        filteredDevices = [dev for dev in devices if dev.serial == serial]

        # Check if we found something
        if len(filteredDevices):
            # Return the first one, should not have more than one if the same SN!
            device = filteredDevices[0]

            # Check if we should set it as the current / default device
            if setAsCurrent:
                self.device = device

            return device

        # Nothing found
        raise WaveForms.DeviceNotFound(f"The device {serial} was not found!")
    
    def readDeviceStatistics(self):
        if self.analog is None or self.analog.dwf_aio is None:
            raise Exception("Please connect to a device first!")
        
        self.analog.dwf_aio.status()
        usbVoltage = self.analog.dwf_aio.channelNodeStatus(2, 0)
        usbCurrent = self.analog.dwf_aio.channelNodeStatus(2, 1)
        temperature = self.analog.dwf_aio.channelNodeStatus(2, 2)
        auxVoltage = self.analog.dwf_aio.channelNodeStatus(3, 0)
        auxCurrent = self.analog.dwf_aio.channelNodeStatus(3, 1)

        return WaveForms.DeviceStats(
            usbVoltage=usbVoltage,
            usbCurrent=usbCurrent,
            auxVoltage=auxVoltage,
            auxCurrent=auxCurrent,
            temperature=temperature,
        )
    class DeviceStats(NamedTuple):
        usbVoltage: float
        usbCurrent: float
        auxVoltage: float
        auxCurrent: float
        temperature: float

class AnalogWaveforms:
    class AnalogFunction(Enum):
        SINE = dwf.DwfAnalogOut.FUNC.SINE
        SQUARE = dwf.DwfAnalogOut.FUNC.SQUARE
        TRIANGLE = dwf.DwfAnalogOut.FUNC.TRIANGLE
        CUSTOM = dwf.DwfAnalogOut.FUNC.CUSTOM
        DC = dwf.DwfAnalogOut.FUNC.DC
        NOISE = dwf.DwfAnalogOut.FUNC.NOISE
        PLAY = dwf.DwfAnalogOut.FUNC.PLAY
        RAMP_DOWN = dwf.DwfAnalogOut.FUNC.RAMP_DOWN
        RAMP_UP = dwf.DwfAnalogOut.FUNC.RAMP_UP


    class VoltageRange(Enum):
        PM2_5 = 2.5  # ±2.5V range
        PM25 = 25.0  # ±25V range


    class RecordChannelConfig(NamedTuple):
        voltageRange: "VoltageRange"
        channelID: int  # The ID of the channel (0,1,2,..)
        enabled: bool   # If this channel is enabled
        offset: float   # Offset in [V]


    class SamplesLost(Exception): pass
    class SamplesCorrupt(Exception): pass

    def __init__(self, deviceIndex: WaveForms.Device):
        # Set the device index
        self.dwf_aio: Optional[dwf.DwfAnalogIO] = None
        self.dwf_ao: Optional[dwf.DwfAnalogOut] = None
        self.dwf_ai: Optional[dwf.DwfAnalogIn] = None
        self.deviceIndex = deviceIndex

        # prepare
        self.analogDataStream: Optional[Signal] = None
        self.liveDataProcess: Optional[threading.Process] = None # TODO: Fix name
        self.liveDataStopEvent: Optional[threading.Event] = None

    def close(self):
        if self.dwf_aio is not None:
            self.dwf_aio.close()

    @property
    def deviceIndex(self) -> WaveForms.Device:
        return self._deviceIndex

    @deviceIndex.setter
    def deviceIndex(self, x: WaveForms.Device):
        # Set the new value
        self._deviceIndex = x

        # Update the ao reference
        try:
            dwfDevice = dwf.Dwf(x.device)

            self.dwf_aio = dwf.DwfAnalogIO(dwfDevice)
            self.dwf_ao = dwf.DwfAnalogOut(dwfDevice)
            self.dwf_ai = dwf.DwfAnalogIn(dwfDevice)

        except dwf.DWFError as e:
            print(f"Encountered dwf error: {e}")

            raise WaveForms.DWFError(str(e))

    def channelCount(self):
        raise NotImplementedError("Has not been implemented!")

    def configureOutput(
        self,
        channel: int,
        signal: Union[Signal, AnalogFunction],
        frequency: Optional[float] = None,
        amplitude: Optional[float] = None,
        offset: Optional[float] = None,
        node: dwf.DwfAnalogOut.NODE = dwf.DwfAnalogOut.NODE.CARRIER,
        start: bool = False,
    ):
        """
        Configure an analog output
        
        Parameters
        ----------
        channel : int
            The channel number
        signal : Union[Signal, AnalogFunction]
            The signal or a function that is built into the Analog Discovery
        frequency : float
            The frequency of the signal in [Hz]
        amplitude : float
            The amplitude of the signal in [V]
        offset : float
            The signal offset in [V]
        node : dwf.DwfAnalogOut.NODE, optional
            Node, by default dwf.DwfAnalogOut.NODE.CARRIER
        start : bool, optional
            If we should automatically start the signal, by default False
        """
        if self.dwf_ao is None:
            raise Exception("Please initialize device first!")

        # Set the Output to be enabled
        self.dwf_ao.nodeEnableSet(channel, node, True)

        # Check the signal type
        if isinstance(signal, Signal):
            # Check the count of sample points
            samplePoints = len(signal.signal)
            if samplePoints > WaveForms.MAX_SAMPLE_POINTS:
                raise Exception(
                    f"Too many sample points provided: {samplePoints} (Max: {WaveForms.MAX_SAMPLE_POINTS})"
                )

            # Check for an offset
            if offset is None:
                offset = np.mean(signal.signal)

            if frequency is None:
                frequency = 1 / signal.time[-1]

            if amplitude is None:
                amplitude = np.max(signal.signal)

            # Custom signal, set the max to one
            sig = signal.signal / np.max(signal.signal)
            self.dwf_ao.nodeDataSet(channel, node, sig)

            # Set it to be a custom signal
            sigFunction = AnalogWaveforms.AnalogFunction.CUSTOM

        elif isinstance(signal, AnalogWaveforms.AnalogFunction):
            sigFunction = signal

        else:
            raise Exception(f"Can't load a {signal} into the Analog Discovery!")

        # Set the signal function and other params
        self.dwf_ao.nodeFunctionSet(channel, node, sigFunction.value)
        self.dwf_ao.nodeFrequencySet(channel, node, frequency)
        self.dwf_ao.nodeOffsetSet(channel, node, offset)
        self.dwf_ao.nodeAmplitudeSet(channel, node, amplitude)

        # Write the config, set if we should start
        self.dwf_ao.configure(channel, start)

    def startOutput(self, channel: int):
        if self.dwf_ao is None:
            raise Exception("dwf_ao not initialized!")

        self.dwf_ao.configure(channel, True)

    def status(self):
        raise NotImplementedError("Has not been implemented")

    def stopOutput(self, channel: int, reset: bool = True):
        """
        Stop the output
        
        Parameters
        ----------
        channel : int
            Which channel we sould like to stop

        reset : bool
            If we should completley reset the channel. The output will not completley switch off until it's reset, by default True
        """
        if self.dwf_ao is None:
            raise Exception("dwf_ao not initialized!")

        self.dwf_ao.configure(channel, False)

        # Should we reset?
        if reset:
            self.dwf_ao.reset(channel)

    def record(
        self,
        frequency: float,
        samples: int,
        channelConfig: List[RecordChannelConfig],
        stopEvent:Optional[threading.Event]
    ) -> Optional[Dict[int, Signal]]:
        """
        Record data on a analog channel. Recording time = samples/frequency
    
        
        Parameters
        ----------
        frequency : float
            At which frequency to make the readings [Hz]

        samples : int
            The number of samples to record
        
        channelConfig : RecordChannelConfig
            Channel configuration

        stopEvent : Optional[threading.Event]
            Optional threading event, to stop the measurement

        Returns
        -------
        Optional[Dict[int, Signal]]
            The recorded signal or nothing, if the measurement was stopped. 
            The dict contains the channel number and the signal as the value
        """

        if self.dwf_ai is None:
            raise Exception("dwf_ai not initialized!")

        # configure each channel
        for channelcfg in channelConfig:
            self.dwf_ai.channelEnableSet(channelcfg.channelID, channelcfg.enabled)
            self.dwf_ai.channelOffsetSet(channelcfg.channelID, channelcfg.offset)
            self.dwf_ai.channelRangeSet(channelcfg.channelID, channelcfg.voltageRange.value)

        # Set into record mode
        self.dwf_ai.acquisitionModeSet(self.dwf_ai.ACQMODE.RECORD)

        # Update the frequency / length
        self.dwf_ai.frequencySet(frequency)
        self.dwf_ai.recordLengthSet(samples / frequency)

        # begin acquisition
        self.dwf_ai.configure(False, True)
        print("waiting to finish recording...")

        # Init
        rgdSamples: List[List[float]] = []

        # Prefill
        for _ in channelConfig: rgdSamples.append([])

        cSamples = 0
        cCorrupted = 0
        cLost = 0
        fLost = False
        fCorrupted = False

        while cSamples < samples:
            # Check if we should continue
            if stopEvent and stopEvent.is_set():
                return None

            sts = self.dwf_ai.status(True)
            if cSamples == 0 and sts in (
                self.dwf_ai.STATE.CONFIG,
                self.dwf_ai.STATE.PREFILL,
                self.dwf_ai.STATE.ARMED,
            ):
                # Acquisition not yet started.
                continue

            cAvailable, cLost, cCorrupted = self.dwf_ai.statusRecord()
            cSamples += cLost

            # Check if we are done
            if sts == self.dwf_ai.STATE.DONE and cAvailable == 0:
                break

            if cLost > 0:
                fLost = True

            if cCorrupted > 0:
                fCorrupted = True

            if cAvailable == 0:
                continue

            if cSamples + cAvailable > samples:
                cAvailable = samples - cSamples

            # get samples
            for channel in channelConfig:
                rgdSamples[channel.channelID].extend(self.dwf_ai.statusData(channel.channelID, cAvailable))

            cSamples += cAvailable

        if fLost:
            raise AnalogWaveforms.SamplesLost("Samples were lost! Reduce frequency")

        if cCorrupted:
            raise AnalogWaveforms.SamplesCorrupt(
                "Samples could be corrupted! Reduce frequency"
            )

        inSignals: Dict[int, Signal] = {}
        cutoffLen = 1

        for channel in channelConfig:
            # Create a signal, remove the first measurement as it's faulty
            sig = rgdSamples[channel.channelID][cutoffLen:]

            # Generate the time. Sometimes, we have a shorter signal, so cut it
            time = np.linspace(0, samples / frequency, len(sig))

            inSignals[channel.channelID] = Signal(
                "Recorded",
                time,
                sig,
            )

        # Return the signal
        return inSignals

    def liveData(
        self,
        frequency: float,
        samples: int,
        channelConfig: List[RecordChannelConfig],
        onUpdate: Callable[[Signal], None]
    ) -> Signal:
        if self.dwf_ai is None:
            raise Exception("dwf_ai not initialized!")

        # configure each channel
        channels: List[int] = []
        self.liveDataSamples = {}
        for channelcfg in channelConfig:
            self.dwf_ai.channelEnableSet(channelcfg.channelID, channelcfg.enabled)
            self.dwf_ai.channelRangeSet(channelcfg.channelID, channelcfg.voltageRange.value)
            channels.append(channelcfg.channelID)

            # Reset the sample numbers
            self.liveDataSamples[channelcfg.channelID] = 0

        # Setup aquisition
        self.dwf_ai.acquisitionModeSet(self.dwf_ai.ACQMODE.SCAN_SHIFT)
        self.dwf_ai.frequencySet(frequency)
        self.dwf_ai.bufferSizeSet(samples)

        # Threading
        self.liveDataStopEvent = threading.Event()
        self.liveDataQueue = Queue()
        self.liveDataSampleTime = 1/frequency
        self.liveDataBufferSize = samples

        fetchNewLiveDataThread = threading.Thread(
            target=self._fetchNewLiveData,
            kwargs={
                "channels": channels,
            })
        fetchNewLiveDataThread.start()

        liveDataProcessorThread = threading.Thread(
            target=self._liveDataProcessor,
            kwargs={
                "onUpdate": onUpdate
            }
        )
        liveDataProcessorThread.start()

    
    def _liveDataProcessor(
        self,
        onUpdate: Callable[[Signal], None]
    ):
        if not self.liveDataStopEvent:
            print("No stopEvent defined!")
            return

        # Loop
        while not self.liveDataStopEvent.wait(0.1):
            # Check if we have new data
            if not self.liveDataQueue.empty():
                # Get a new sample
                samples = self.liveDataQueue.get()
                chopAwayLen = 10

                # Process
                rgdSamples:Dict[int, Signal] = {}
                
                for ch, signal in samples.items():
                    signal = signal[chopAwayLen:]
                    sigLen = len(signal)

                    # Calc the time axis
                    endTime = sigLen * self.liveDataSampleTime
                    time = np.linspace(0, endTime, sigLen)

                    # Create the signal
                    rgdSamples[ch] = Signal("", time, signal)

                # update!
                onUpdate(rgdSamples)


    def _fetchNewLiveData(
        self,
        channels: List[int],
    ):
        """
        Fetch new data from the AD
        
        Raises
        ------
        Exception
            when the dwf_ai is not initialized
        """
        if self.dwf_ai is None:
            raise Exception("dwf_ai not initialized!")
        
        # Check if we have an event
        if not self.liveDataStopEvent:
            print("No self.liveDataStopEvent defined!")
            return

        # begin acquisition
        self.dwf_ai.configure(False, True)

        # Prepare
        rgdSamples: Dict[int, np.array] = {}

        # Loop
        while not self.liveDataStopEvent.is_set():

            sts = self.dwf_ai.status(True)
            cValid = self.dwf_ai.statusSamplesValid()

            # get samples
            for channel in channels:
                rgdSamples[channel] = np.array(self.dwf_ai.statusData(channel, cValid))

            # Add new data
            self.liveDataQueue.put(rgdSamples)

    def stopLiveData(self) -> bool:
        """
        Stop live data recording
        
        Returns
        -------
        bool
            If we stopped the process
        """
        if self.liveDataStopEvent is not None:
            self.liveDataStopEvent.set()
            return True
        
        return False

    def resetAnalogOut(self, channel=-1, parent=None):
        if self.dwf_ao is None:
            raise Exception("dwf_ao not initialized!")
        
        # Reset
        self.dwf_ao.reset(idxChannel=channel, parent=parent)

    def resetAnalogIn(self, parent=None):
        if self.dwf_ai is None:
            raise Exception("dwf_ai not initialized!")
        
        # Reset
        self.dwf_ai.reset(parent=parent)

    def setupTrigger(
        self,
        channel:int,
        level:float,
        triggerType:dwf.DwfAnalogIn.TRIGTYPE = dwf.DwfAnalogIn.TRIGTYPE.EDGE,
        condition:dwf.DwfAnalogIn.TRIGCOND = dwf.DwfAnalogIn.TRIGCOND.RISING_POSITIVE,
        source:dwf.DwfAnalogIn.TRIGSRC = dwf.DwfAnalogIn.TRIGSRC.DETECTOR_ANALOG_IN
    ):
        """
        Sets up the analog trigger
        
        Parameters
        ----------
        channel : int
            Which channel to trigger on

        level : float
            At which voltage level to trigger in [V]

        triggerType : dwf_ai.TRIGTYPE
            The triggering type, by default dwf.DwfAnalogIn.TRIGTYPE.EDGE

        condition : dwf_ai.TRIGCOND
            Triggering condition, by default dwf.DwfAnalogIn.TRIGCOND.RISING_POSITIVE

        source : dwf_ai.TRIGSRC, optional
            Trigger source, by default dwf.DwfAnalogIn.TRIGSRC.DETECTOR_ANALOG_IN
        """
        if self.dwf_ai is None:
            raise Exception("dwf_ai not initialized!")

        #set up trigger
        self.dwf_ai.triggerAutoTimeoutSet() #disable auto trigger
        self.dwf_ai.triggerSourceSet(source)
        self.dwf_ai.triggerTypeSet(triggerType)
        self.dwf_ai.triggerChannelSet(channel)
        self.dwf_ai.triggerLevelSet(level)
        self.dwf_ai.triggerConditionSet(condition)

    def setupOutputSynchronization(self, masterChannel: int, slaveChannel: int):
        if self.dwf_ao is None:
            raise Exception("dwf_ao not initialized!")

        # for second channel set master the first channel
        self.dwf_ao.masterSet(slaveChannel, masterChannel)

    # TODO: Move this into the signal class! 
    def phaseShiftCalculation(self, signal1, signal2):
        pass

class DigitalWaveforms:
    def __init__(self, deviceIndex: Optional[WaveForms.Device] = None):
        self.deviceIndex = deviceIndex
        self.dwf_do: Optional[dwf.DwfDigitalOut]= None

    def close(self):
        if self.dwf_do is not None:
            self.dwf_do.close()

    @property
    def deviceIndex(self) -> Optional[WaveForms.Device]:
        return self._deviceIndex

    @deviceIndex.setter
    def deviceIndex(self, x: Optional[WaveForms.Device]):
        # Set the new value
        self._deviceIndex = x

        try:
            # Update the do reference
            self.dwf_do = dwf.DwfDigitalOut(x.device)

        except dwf.DWFError as e:
            print(f"Encountered dwf error: {e}")

            raise WaveForms.DWFError(str(e))

    def startOutput(self, signal: Signal):
        if self.dwf_do is None:
            raise Exception("Please initialize device first!")

    def stopOutput(self):
        pass

    def record(self):
        pass
