class SignalException(Exception):
    pass


class UnsupportedSignal(Exception):
    pass


class UnsupportedOperation(Exception):
    pass


class SignalsDifferentShapes(Exception):
    pass


class UnsupportedSignalType(Exception):
    pass


class ProcessingError(Exception):
    pass
