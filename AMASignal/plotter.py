# Type checking
from __future__ import annotations
from typing import Union, TYPE_CHECKING, List, cast, Dict

if TYPE_CHECKING:
    pass

# Other imports
import matplotlib.pyplot as plt
import numpy as np

# Custom imports
from AMASignal.signal import Signal
from AMASignal.exceptions import UnsupportedSignalType

plt.style.use("seaborn-whitegrid")


class Cycler:
    def __init__(self, value):
        self.__value = value
        self.__index = 0

    @property
    def value(self):
        # Check if the colour is a list
        if type(self.__value) is list:
            # Yes, so get the latest colour
            colour = self.__value[self.__index]
            self.__index += 1

            # Check if we need to reset
            if self.__index >= len(self.__value):
                self.__index = 0

            return colour

        # Return the normal colour
        return self.__value

    @value.setter
    def value(self, value):
        self.__value = value
        self.__index = 0


class Plotter:
    class LineStyle:
        colourCyclerColours = ["k", "r", "g", "m", "b", "c", "y"]
        colourCyclerColours2 = [
            "#407db8",
            "#90b337",
            "#7b8280",
            "#c94038",
            "#7c69a5",
            "#47342e",
            "#f6ce25",
            "#274d6f",
            "#0f0e13",
        ]
        markerSizeCyclerSizes = [10, 5, 15, 20, 5]

        def __init__(
            self,
            lw=None,
            ls=None,
            colour=None,
            linestyle=None,
            marker=None,
            markersize=None,
            markerfacecolor=None,
            markeredgewidth=None,
            markeredgecolor=None,
            alpha=1,
            s=None,
        ):
            self.lw = lw
            self.ls = ls
            self.linestyle = linestyle
            self.marker = marker
            self.markerfacecolor = markerfacecolor
            self.markeredgewidth = markeredgewidth
            self.alpha = alpha

            self.__colour = Cycler(colour)
            self.__markersize = Cycler(markersize)
            self.__markeredgecolor = Cycler(markeredgecolor)
            self.__s = Cycler(s)

        @property
        def colour(self):
            return self.__colour.value

        @colour.setter
        def colour(self, colour):
            self.__colour.value = colour

        @property
        def markersize(self):
            return self.__markersize.value

        @markersize.setter
        def markersize(self, markersize):
            self.__markersize.value = markersize

        @property
        def markeredgecolor(self):
            return self.__markeredgecolor.value

        @markeredgecolor.setter
        def markeredgecolor(self, markeredgecolor):
            self.__markeredgecolor.value = markeredgecolor

        @property
        def s(self):
            return self.__s.value

        @s.setter
        def s(self, s):
            self.__s.value = s

        def updateKwargs(self, kwargs):
            # Prepare all the args that we can update
            args = {
                "lw": self.lw,
                "ls": self.ls,
                "color": self.colour,
                "linestyle": self.linestyle,
                "marker": self.marker,
                "markersize": self.markersize,
                "markerfacecolor": self.markerfacecolor,
                "markeredgewidth": self.markeredgewidth,
                "markeredgecolor": self.markeredgecolor,
                "alpha": self.alpha,
                "s": self.s,
            }

            # Check if they are not set to none
            for arg, value in args.items():
                if value is not None:
                    kwargs[arg] = value

            # Return the updated args
            return kwargs

    simpleLines = (
        LineStyle(lw=2, ls="-", marker="", colour="b", markersize=3, alpha=1.0),
        LineStyle(lw=1, ls="", marker="o", markersize=5, colour="b", alpha=1.0),
        LineStyle(
            lw=1, ls="-", marker="", colour=LineStyle.colourCyclerColours2, alpha=1
        ),
        LineStyle(
            lw=1, ls="-", marker="", colour=LineStyle.colourCyclerColours, alpha=0.5
        ),
        LineStyle(
            lw=1,
            ls="",
            marker="o",
            markersize=LineStyle.markerSizeCyclerSizes,
            markerfacecolor="None",
            markeredgewidth=2,
            markeredgecolor="blue",
            alpha=1.0,
        ),
        LineStyle(
            lw=1,
            ls="",
            marker="o",
            markersize=LineStyle.markerSizeCyclerSizes,
            markerfacecolor="None",
            markeredgewidth=2,
            markeredgecolor="magenta",
            alpha=1.0,
        ),
        LineStyle(
            lw=1,
            ls="",
            marker="o",
            markersize=LineStyle.markerSizeCyclerSizes,
            markerfacecolor="None",
            markeredgewidth=2,
            markeredgecolor="yellow",
            alpha=1.0,
        ),
        LineStyle(
            lw=1,
            ls="",
            marker="o",
            markersize=LineStyle.markerSizeCyclerSizes,
            colour=LineStyle.colourCyclerColours,
            alpha=1.0,
        ),
        LineStyle(lw=1, ls="--", colour="blue", alpha=0.25),
    )

    scatterStyles = (
        LineStyle(
            lw=1,
            # ls="",
            marker="o",
            # markersize=LineStyle.markerSizeCyclerSizes,
            # markerfacecolor="None",
            # markeredgewidth=2,
            # markeredgecolor=LineStyle.colourCyclerColours,
            alpha=1.0,
        ),
    )

    hlineStyles = (LineStyle(linestyle="-", colour="grey", alpha=0.5),)

    fillStyle = (LineStyle(colour="grey", lw=0, alpha=0.25),)

    def __init__(self):
        self.width = 4
        self.height = 1
        self.dpi = 200


class GeneralAxes(plt.Axes):
    name = "GeneralAxes"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.set_ylabel("Amplitude")
        self.set_xlabel("Time [s]")
        self.set_title("A Signal!")

        # Prepare a x values container
        self.__XValues = []

    def styleGrid(self):
        self.grid(which="both", color="k", alpha=0.75, ls="-", lw=0.5)

    def plotMainSignal(
        self, signal: Signal, y=None, style=None, logX: bool = False, **kwargs
    ):
        # Set the title
        self.set_title(f"A {signal.name} Signal!")
        self.__XValues = signal.time

        self.set_ylabel(signal.signalName)
        self.set_xlabel(signal.timeName)

        if logX:
            self.set_xscale("log")
            self.grid(color="k", alpha=0.2, ls="-", lw=0.5, which="minor")

        # Check which y data to use
        sig = None
        if y is not None:
            sig = y
        else:
            sig = signal.signal

        try:
            if np.iscomplex(sig[0]):
                sig = np.abs(sig)
        except IndexError as e:
            print("Skipping complex check:", e)

        # Check if the signal is a signal or just points
        if signal.type == Signal.Types.scatter:
            # Points to plot
            # Check if we have a plot style
            style = Plotter.scatterStyles[0] if style is None else style
            updatedKwargs = style.updateKwargs(kwargs)

            # Plot
            super().scatter(self.__XValues, sig, label=signal.name, **updatedKwargs)

        elif signal.type == Signal.Types.line or signal.type == Signal.Types.hline:
            # A line signal
            # Check if we have a plot style
            style = Plotter.simpleLines[2] if style is None else style
            updatedKwargs = style.updateKwargs(kwargs)

            # Plot
            super().plot(self.__XValues, sig, label=signal.name, **updatedKwargs)

        else:
            # Unknown
            raise UnsupportedSignalType(
                f"The signal type {signal.type} is not supported!"
            )

        self.legend()

    def plotHelperSignals(
        self, yValues: Dict[str, Union[np.ndarray, Signal]], style=None, **kwargs
    ):
        # Check if we have a plot style
        stylePlot = Plotter.simpleLines[2] if style is None else style
        styleScatter = Plotter.scatterStyles[0] if style is None else style

        # Convert it to a list if it isn't
        if type(yValues) is not dict:
            yValues = {"plt_one_signal": yValues}

        # Plot each one
        for name, y in yValues.items():
            x = self.__XValues
            plotType = Signal.Types.line
            signal = None

            if isinstance(y, Signal):
                # Get some data
                signal = cast(Signal, signal)
                signal = y
                name = signal.name
                x = signal.time
                plotType = signal.type
                y = signal.signal

            if plotType == Signal.Types.scatter:
                updatedKwargs = styleScatter.updateKwargs(kwargs)

                # Plot
                super().plot(x, y, label=name, **updatedKwargs)

            elif plotType == Signal.Types.line:
                updatedKwargs = stylePlot.updateKwargs(kwargs)

                # Check if we have some complex data
                if np.iscomplex(y[0]):
                    y = np.abs(y)

                super().plot(x, y, label=name, **updatedKwargs)

            elif plotType == Signal.Types.hline:
                updatedKwargs = stylePlot.updateKwargs(kwargs)

                super().axhline(y, label=name, **updatedKwargs)

            elif plotType == Signal.Types.std:
                updatedKwargs = stylePlot.updateKwargs(kwargs)

                self.std(x, y[0], y[-1], label=name)

            else:
                # Unknown
                raise UnsupportedSignalType(
                    f"The signal type {signal} is not supported!"
                )

        self.legend()

    def plotHelperHLine(self, yPoint, style=None, **kwargs):
        # Check if we have a plot style
        style = Plotter.hlineStyles[0] if style is None else style
        updatedKwargs = style.updateKwargs(kwargs)

        super().hlines(
            yPoint, xmin=self.__XValues[0], xmax=self.__XValues[-1], **updatedKwargs
        )

        self.legend()

    def std(self, x, y1, y2, label=None, style=None):
        # Check if we have a plot style
        style = Plotter.fillStyle[0] if style is None else style

        # Fill
        super().fill_between(
            x, y1, y2, color=style.colour, lw=style.lw, alpha=style.alpha, label=label
        )

        self.legend()

    def axvline(self, x, ymin=0, ymax=1, **kwargs):
        """[summary]
        
        Arguments:
            x {[type]} -- [description]
        
        Keyword Arguments:
            ymin {int} -- [description] (default: {0})
            ymax {int} -- [description] (default: {1})
        """
        for i, value in enumerate(x):
            # Check if we should remove the label
            if i > 0 and "label" in kwargs:
                del kwargs["label"]

            super().axvline(value, ymin=ymin, ymax=ymax, **kwargs)


if __name__ == "__main__":
    import matplotlib.projections as proj

    proj.register_projection(GeneralAxes)

    fig, axes = plt.subplots(2, 2, subplot_kw=dict(projection="GeneralAxes"))
    axes[0][0].plot(np.array([1, 2, 3]), np.array([1, 2, 3]))

    plt.show()
