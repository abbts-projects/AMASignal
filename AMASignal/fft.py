"""
Package for FFT
"""

# Standard imports
from typing import Union, cast, Optional, Dict, Callable

# 3rd Party imports
from scipy.fftpack import fft
from scipy import signal as scipySignal
import numpy as np
from enum import Enum, auto

# Custom imports
from AMASignal import Processing
from AMASignal import Signal


class FFT:
    def ceilBase2(self, samples: int) -> int:
        """
        Find find the next highest number with base 2 to the input
        
        Parameters
        ----------
        samples : int
            How many samples (measurement points) we currently have
        
        Returns
        -------
        int
            Next highest number with base two to the input
        """
        return 2 ** np.ceil(np.log2(samples))

    def resampleSignal(
        self,
        signal: Signal,
        method: Processing.Interpolate.Method = Processing.Interpolate.Method.linear,
    ) -> Signal:
        """
        Resample the given signal to the next base 2
        
        Parameters
        ----------
        signal : Signal
            The Signal we would like to interpolate
        method : Processing.Interpolate.Method, optional
            The method used to interpolate, by default Processing.Interpolate.Method.linear

        Returns
        -------
        Signal
            Returns a new, interpolated signal
        """
        # Get the new sample count
        newSamples = self.ceilBase2(len(signal.time))

        # Process the signal and return it
        sig = signal.process(Processing.Interpolate(n=newSamples, method=method))
        sig = cast(Signal, sig)

        return sig

    def getDCOffset(self, signal: Signal) -> float:
        """
        Get the DC offset of a signal
        
        Parameters
        ----------
        signal : Signal
            The signal to get the offset of
        
        Returns
        -------
        float
            The DC offset of the signal
        """
        # Get the mean of the signal and return it
        return np.mean(signal.signal)

    def removeDCOffset(self, signal: Signal) -> Signal:
        """
        Remove the DC offset of a signal and return a new one
        
        Parameters
        ----------
        signal : Signal
            The signal where we need to remove the DC offset
        
        Returns
        -------
        Signal
            A new signal without the DC offset
        """
        # Get the offset
        offset = self.getDCOffset(signal)

        # Create the new signal
        noOffset = signal.signal - offset
        sig = Signal(f"{signal.name} w/o DC offset", signal.time, noOffset)

        # Return the signal
        return sig

    def applyWindow(
        self,
        signal: Signal,
        window: Optional[
            Union[np.ndarray, int, float, Callable[[int], np.ndarray]]
        ] = None,
    ) -> Signal:
        """
        Apply a window to the signal
        
        Parameters
        ----------
        signal : Signal
            The signal to apply the window to

        window : Optional[Union[np.ndarray, int, float, Callable[[int], np.ndarray]]]
            Which window function to apply based on an array, by default scipy.windows.hamming


            Available windows
            -----------------
            SciPy: https://docs.scipy.org/doc/scipy/reference/signal.windows.html

            Numpy:
            - np.hanning(M)
            - np.kaiser(M, beta)
            - np.hamming(M)
            - np.blackman(M)
            - np.bartlett(M)
        
        Returns
        -------
        Signal
            A new signal with the applied window
        """
        # Check which type we want to use
        if window is None:
            window = scipySignal.windows.hamming

        # Apply the window
        if callable(window):
            appliedSignal = window(len(signal.signal)) * signal.signal
        else:
            appliedSignal = window * signal.signal

        # Create a new signal
        sig = Signal(f"{signal.name} w/ window", signal.time, appliedSignal)

        # Return it
        return sig

    def getFFT(self, signal: Signal, fLimit: Optional[int] = None) -> Signal:
        # Do the FFT calculation
        measCount = len(signal.signal)
        fftSpectrumTotal = np.abs(fft(signal.signal)) * 2 / measCount
        fftSpectrumTotal[0] = fftSpectrumTotal[0] / 2

        # Get the time axis
        timeDelta = signal.timedelta(returnMean=True)
        fftFreq = np.linspace(0, (1 / (2 * timeDelta)), measCount // 2)

        # Get the spectrum
        fftSpectrum = fftSpectrumTotal[0 : measCount // 2]

        # Do some filtering
        fLimit = fLimit if fLimit is not None else fftFreq[-1]
        usableData = fftFreq <= fLimit
        fftFreq = fftFreq[usableData]
        fftSpectrum = fftSpectrum[usableData]

        # Create a signal
        sig = Signal("FFT", fftFreq, fftSpectrum)
        sig.timeName = "Frequency [Hz]"
        sig.signalName = "Amplitude"

        # return the signal
        return sig

    def getConditionedFFT(
        self,
        signal: Signal,
        window: Optional[
            Union[np.ndarray, int, float, Callable[[int], np.ndarray]]
        ] = None,
        fLimit: Optional[int] = None,
    ) -> Signal:
        # Resample the signal to have a base 2 number of samples
        resampledSignal = self.resampleSignal(signal)

        # Get the DC offset
        dcOffset = self.getDCOffset(signal)

        # Remove the DC offset
        resampledSignal = resampledSignal - dcOffset

        # Apply the window
        appliedWindow = self.applyWindow(resampledSignal, window)

        # Re add the DC offset
        appliedWindowWithOffset = appliedWindow + dcOffset

        # Return the FFT data
        return self.getFFT(appliedWindowWithOffset, fLimit)
