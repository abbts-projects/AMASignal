# Type checking
from __future__ import annotations
from typing import Union, TYPE_CHECKING, List, NoReturn, cast, Callable

# 3rd Party imports
import numpy as np
from scipy import signal as scipySignal
from scipy import interpolate
from enum import Enum, auto

# Other imports
from .signal import Signal
from .exceptions import ProcessingError


class Processing:
    """
    Signal Processsing wrapper
    """

    class Signalprocessor:
        """
        Signal processor. Meant to be subclassed

        Parameters
        ----------
        name : str, optional
            Name of the processor, by default None

        Raises
        ------
        Exception
            Not implemented
        """

        def __init__(self, name: str = None):
            self.outputData = None
            self.processorName = getattr(self, "processorName", "Processor")

            self.name = name if name is not None else self.processorName

        def processSignal(self, signal: Signal) -> Union[Signal, List[Signal]]:
            """
            Process a signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed

            Returns
            -------
            Union[Signal, List[Signal]]
                The processed signal or a list of processed signals
            """
            pass

        @staticmethod
        def zeroCrossings(signal: np.ndarray, after: bool = False) -> np.ndarray:
            """
            Get the zero crossings
            
            Parameters
            ----------
            signal : np.ndarray
                The signal in an array
            after : bool, optional
                If we should get the points after the zero crossing, by default False
            
            Returns
            -------
            np.ndarray
                Indicies of the zero crossings
            """
            return np.where(np.diff(np.sign(signal)))[0] + after

        @staticmethod
        def signalFromIndices(
            signal: Signal,
            indices: np.ndarray,
            name: str,
            sigType: Signal.Types = Signal.Types.line,
        ) -> Signal:
            """
            Create a signal from indicies
            
            Parameters
            ----------
            signal : Signal
                The original signal

            indices : np.ndarray
                The indicies to be extracted

            name : str
                The new name of the signal

            sigType : Signal.Types, optional
                The signal type, by default Signal.Types.line
            
            Returns
            -------
            Signal
                The new signal
            """
            # Get the values
            values = signal.signal[indices]
            times = signal.time[indices]

            # Create a new signal
            sig = Signal(name, times, values)
            sig.type = sigType

            # Return it
            return sig

        @staticmethod
        def signalFromValues(
            signal: Signal,
            values: np.ndarray,
            name: str,
            sigType: Signal.Types = Signal.Types.line,
        ) -> Signal:
            """
            Creates a new signal from the given values
            
            Parameters
            ----------
            signal : Signal
                the original signal

            values : np.ndarray
                The new values for the y axis

            name : str
                The new name of the signal

            sigType : Signal.Types, optional
                The type of signal, by default Signal.Types.line
            
            Returns
            -------
            Signal
                The new signal
            """
            # Get the time
            times = signal.time

            # Create a new signal
            sig = Signal(name, times, values)
            sig.type = sigType

            # Return it
            return sig

    class Max(Signalprocessor):
        """
        Max value(s) of a signal
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Max"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            # Get the max indices
            maxIndexes = np.where(signal.signal == np.amax(signal.signal))

            # Create the signal
            return Processing.Signalprocessor.signalFromIndices(
                signal, maxIndexes, self.name, sigType=Signal.Types.scatter
            )

    class LocalMax(Signalprocessor):
        """
        Local max values
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Local Max"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            # Get the local max indices
            localMaxIndices = scipySignal.argrelextrema(signal.signal, np.greater)

            # Create the signal
            return Processing.Signalprocessor.signalFromIndices(
                signal, localMaxIndices, self.name, sigType=Signal.Types.scatter
            )

    class Min(Signalprocessor):
        """
        Min value(s) of a signal
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Min"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            # Get the min indices
            minIndexes = np.where(signal.signal == np.amin(signal.signal))

            # Create the signal
            return Processing.Signalprocessor.signalFromIndices(
                signal, minIndexes, self.name, sigType=Signal.Types.scatter
            )

    class LocalMin(Signalprocessor):
        """
        Local minimums of a signal
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Local Min"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            # Get the local max indices
            localMinIndices = scipySignal.argrelextrema(signal.signal, np.less)

            # Create the signal
            return Processing.Signalprocessor.signalFromIndices(
                signal, localMinIndices, self.name, sigType=Signal.Types.scatter
            )

    class InflectionPoint(Signalprocessor):
        """
        Get the inflection points of a signal
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Inflection point"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            # Get the zero crossings
            diffSig = Processing.Differential(n=2).processSignal(signal)
            inflections = Processing.Signalprocessor.zeroCrossings(diffSig.signal)

            # Create the signal
            return Processing.Signalprocessor.signalFromIndices(
                signal, inflections, self.name, sigType=Signal.Types.scatter
            )

    class TangentLine(Signalprocessor):
        """
        A visual tangent line

        Parameters
        ----------
        pointSignal : Signal
            A signal containing points to get the tangent of
        """

        def __init__(self, pointSignal: Signal, *args, **kwargs):
            # Set the name
            self.processorName = "Tangent"

            # Call our parents
            super().__init__(*args, **kwargs)

            # Other
            self.pointSignal = pointSignal

        def processSignal(self, signal: Signal) -> List[Signal]:
            """
            Process the signal to get a tangent line
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            List[Signal]
                A list of signals containing the tangent lines
            
            Raises
            ------
            Exception
                If the input signal is not of type scatter signal
            """
            # check if this is a point signal
            if self.pointSignal.type != Signal.Types.scatter:
                raise Exception("{pointSignal.type} is not a scatter signal!")

            # Get the time
            timeRange = signal.time
            optTime = signal.startFinishTimes

            # Prepare
            signals: List[Signal] = []

            # Loop all the points in this signal
            for x, y in zip(self.pointSignal.time, self.pointSignal.signal):
                # Get the derivative
                diff = cast(Signal, signal.process(Processing.Differential()))
                diffSignal = diff.signal

                # Define tangent line
                # y = m*(x - x1) + y1
                timeIndex = np.where(timeRange == x)[0][0]
                line = diffSignal[timeIndex] * (optTime - x) + y

                # Create a new line and append it
                signals.append(Signal(self.processorName, optTime, line))

            # Return the data
            return signals

    class Gradient(Signalprocessor):
        """
        Get the gradient of a signal
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Gradient"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            # Get the gradient
            gradient = np.gradient(signal.signal)

            # Return the new signal
            return Signal(self.processorName, signal.time, gradient)

    class Std(Signalprocessor):
        """
        Get the standard deviation of a signal
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Std"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            std = np.std(signal.signal)

            offset = Processing.Average().processSignal(signal)

            maxDeviation = offset.signal + std
            minDeviation = offset.signal - std

            # Create the signal
            sig = Signal(self.name, signal.time, (maxDeviation, minDeviation))
            sig.type = Signal.Types.std

            # Return the signal
            return sig

    class Average(Signalprocessor):
        """
        Get the average of a signal
        """

        def __init__(self, *args, **kwargs):
            # Set the name
            self.processorName = "Average"

            # Call our parents
            super().__init__(*args, **kwargs)

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            average = np.average(signal.signal)

            # Create the signal
            sig = Signal(self.name, None, average)
            sig.type = Signal.Types.hline

            # Return the signal
            return sig

    class Downsample(Signalprocessor):
        """
        Downsample a Signal
        
        Parameters
        ----------
        n : int, optional
                Downsample factor, by default 2
        """

        def __init__(self, *args, n=2, **kwargs):
            self.processorName = "Downsampled"

            super().__init__(*args, **kwargs)
            self.n = n

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            # Downsample
            sig = signal.signal[:: self.n]
            tim = signal.time[:: self.n]

            # Create the signal
            sig = Signal(f"{self.name} ({self.n}x)", tim, sig)
            sig.type = Signal.Types.line

            # Return the signal
            return sig

    class MovingAverage(Signalprocessor):
        """
        Moving average

        Methods
        -------
        - simple
        - centered
        - centeredBinom
        - linearWeighted
        
        Parameters
        ----------
        window : int
            The size of the window. Choose how many values should be averaged together
        windowFunction : List[float], optional
            Depending on the method, this is required. Based on the size of the window, the weight of each value, by default None
        method : Union[Method, None], optional
            Averaging method, by default None
        
        Raises
        ------
        ProcessingError
            Window size must be an odd number
        ProcessingError
            Please define a function window. Ex: (0.25, 0.5, 0.25)
        ProcessingError
            Unknown method
        """

        class Method(Enum):
            simple = auto()
            centered = auto()
            centeredBinom = auto()
            linearWeighted = auto()

        def __init__(
            self,
            window: int,
            *args,
            windowFunction: List[float] = None,
            method: Union[Method, None] = None,
            **kwargs,
        ) -> None:
            self.processorName = "Moving average"

            super().__init__(*args, **kwargs)
            self.method = (
                Processing.MovingAverage.Method.simple if method is None else method
            )
            self.window = window
            self.windowFunction = windowFunction

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            windowFunction = None
            averages: List[np.nparray] = []

            # Check the method
            if self.method == Processing.MovingAverage.Method.simple:
                # Return the data
                averages = (
                    np.convolve(signal.signal, np.ones(self.window), mode="same")
                    / self.window
                )

            elif self.method == Processing.MovingAverage.Method.centered:
                if self.window % 2 == 0:
                    raise ProcessingError("Window size must be an odd number")

                shift = int((self.window - 1) / 2)
                count = len(signal.signal)

                for j in range(count):
                    movingSum = 0
                    for i in range(-shift, shift + 1):
                        if 0 <= j + i < count:
                            movingSum += signal.signal[j + i]
                        else:
                            movingSum = 0

                    averages.append(movingSum / self.window)

            elif self.method == Processing.MovingAverage.Method.centeredBinom:
                if windowFunction is None:
                    raise ProcessingError(
                        "Please define a function window. Ex: (0.25, 0.5, 0.25)"
                    )

                # Return the data
                averages = np.convolve(
                    signal.signal, windowFunction, mode="same"
                ) / sum(windowFunction)

            elif self.method == Processing.MovingAverage.Method.linearWeighted:
                windowFunction = [
                    2 * (i + 1) / (self.window * (self.window + 1)) for i in self.window
                ]

                # Fensterfunktion muss gedreht werden, da Funktion convolve 2.
                # Vektor dreht! siehe:
                # https://docs.scipy.org/doc/numpy/reference/generated/numpy.convolve.html
                # Examples: Note how the convolution operator flips the second
                # array before "sliding" the two across one another.
                averages = np.convolve(
                    signal.signal, list(reversed(windowFunction)), mode="same"
                ) / sum(windowFunction)

            else:
                # Whoops ...
                raise ProcessingError(f"Unknown method: {self.method}")

            # Create the signal
            return Processing.Signalprocessor.signalFromValues(
                signal,
                averages,
                f"{self.name} ({self.method})",
                sigType=Signal.Types.line,
            )

    class ZeroCrossings(Signalprocessor):
        """
        Get all the zero crossings of a signal
        
        Methods
        -------
        - before: Get the point before the zero crossing
        - after: Get the point after the zero crossing
        - exact: Get the exact zero crossing

        Parameters
        ----------
        method : Union[Method, None], optional
            Which calculation method should be used
        offset : Union[float, int], optional
            Offset of the zero crossing
        
        Raises
        ------
        Exception
            Unknown method
        """

        class Method(Enum):
            before = auto()
            after = auto()
            exact = auto()

        def __init__(
            self,
            *args,
            method: Union[Method, None] = None,
            offset: Union[float, int] = 0,
            **kwargs,
        ) -> None:
            self.processorName = "Zero crossings"

            super().__init__(*args, **kwargs)
            self.method = (
                method if method is not None else Processing.ZeroCrossings.Method.before
            )
            self.offset = offset

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            # Get the crossings
            crossings = None
            when = ""
            signalGenMethod = "signalFromIndices"

            if self.method == Processing.ZeroCrossings.Method.before:
                crossings = Processing.Signalprocessor.zeroCrossings(
                    signal.signal - self.offset, False
                )
                when = "before"

            elif self.method == Processing.ZeroCrossings.Method.after:
                crossings = Processing.Signalprocessor.zeroCrossings(
                    signal.signal - self.offset, True
                )
                when = "after"

            elif self.method == Processing.ZeroCrossings.Method.exact:
                # Generate a series at zero
                zeros = np.zeros(len(signal.time)) + self.offset
                zeroSignal = Signal("Zero Signal", signal.time, zeros)

                # Now get the intersections
                intersectionSignal = signal.getIntersections(zeroSignal)

                # Check if we have a signal
                if intersectionSignal is None:
                    raise Exception(f"No intersections found!")

                # Get the crossings
                crossings = intersectionSignal.signal
                signalGenMethod = "signalFromValues"
                when = "exact"

            else:
                raise Exception(f"Unknown method {self.method}!")

            # Create the signal
            name = f"{self.name} ({when})"

            # Check which method to use
            if signalGenMethod == "signalFromValues":
                # Generate the signal from the values
                return Processing.Signalprocessor.signalFromValues(
                    signal, crossings, name, sigType=Signal.Types.scatter
                )

            elif signalGenMethod == "signalFromIndices":
                # generate the signals from indicies
                return Processing.Signalprocessor.signalFromIndices(
                    signal, crossings, name, sigType=Signal.Types.scatter
                )

            else:
                # Whoops
                raise Exception(
                    f"Undefined signal generation method: {signalGenMethod}"
                )

    class Differential(Signalprocessor):
        """
        Calculate the differential
        
        Parameters
        ----------
        n : int
            How often to differentiate
        """

        def __init__(self, *args, n: int = 1, **kwargs) -> None:
            self.processorName = "Differential"

            super().__init__(*args, **kwargs)

            self.n = n

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            lastSignal = signal.signal
            count = len(lastSignal)

            for _ in range(self.n):
                dy = [lastSignal[0]]  # 1. Eintrag entspricht dem Funktionswert

                for i in range(1, count):
                    dy.append(
                        (lastSignal[i] - lastSignal[i - 1])
                        / (signal.time[i] - signal.time[i - 1])
                    )

                # Set the last signal
                lastSignal = dy

            # Create the signal
            return Processing.Signalprocessor.signalFromValues(
                signal,
                np.transpose(lastSignal),
                f"{self.name} {self.n}",
                sigType=Signal.Types.line,
            )

    class Integral(Signalprocessor):
        """
        Calculate the Integral
        
        Parameters
        ----------
        method : Union[Method, None], optional
            Which method should be used to calculate the integral, by default None

        Raises
        ------
        ProcessingError
            Unknown method
        """

        class Method(Enum):
            """
            Integral methods
            """

            lower = auto()
            upper = auto()
            trapez = auto()

        def __init__(self, *args, method: Union[Method, None] = None, **kwargs) -> None:
            self.processorName = "Integral"

            super().__init__(*args, **kwargs)

            self.method = Processing.Integral.Method.upper if method is None else method

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            sig = signal.signal

            # Calculate the dx, remove the last one as it's "rolled" around
            dx = np.roll(signal.time, -1) - signal.time

            methodValues = None

            if self.method == Processing.Integral.Method.lower:
                methodValues = signal.signal * dx

            elif self.method == Processing.Integral.Method.upper:
                sig = np.roll(sig, -1)

                methodValues = sig * dx

            elif self.method == Processing.Integral.Method.trapez:
                sig1 = np.roll(sig, -1)

                methodValues = (sig1 + sig) / 2 * dx

            else:
                raise ProcessingError(f"Unknown method: {self.method}")

            # Cumulate everything
            # Remove the last one, as it doesn't contain good info
            cumsum = np.cumsum(methodValues[:-1])
            integral = np.insert(cumsum, 0, 0)

            # Package it
            # Create the signal
            return Processing.Signalprocessor.signalFromValues(
                signal,
                integral,
                f"{self.name} ({self.method})",
                sigType=Signal.Types.line,
            )

    class Interpolate(Signalprocessor):
        """
        Interpolate a signal
        
        Parameters
        ----------
        n : int, optional
            how many points should the interpolation signal have, by default 300
        method : Method, optional
            Which interpolation method should be used, by default None
        """

        class Method(Enum):
            """
            Interpolation methods
            """

            linear = auto()
            nearest = auto()
            zero = auto()
            slinear = auto()
            quadratic = auto()
            cubic = auto()
            previous = auto()
            next = auto()

        def __init__(
            self, *args, n: int = 300, method: Method = None, **kwargs
        ) -> None:
            self.processorName = "Interpolate"

            super().__init__(*args, **kwargs)
            self.n = n
            self.method = (
                Processing.Interpolate.Method.linear if method is None else method
            )

        def processSignal(self, signal: Signal) -> Signal:
            """
            Process the signal
            
            Parameters
            ----------
            signal : Signal
                The signal to be processed
            
            Returns
            -------
            Signal
                New signal with the processed points
            """
            f = interpolate.interp1d(signal.time, signal.signal, kind=self.method.name)
            newX = np.linspace(signal.time[0], signal.time[-1], self.n)
            newY = f(newX)

            # Create the signal
            sig = Signal(f"{self.name} ({self.method.name})", newX, newY)
            sig.type = Signal.Types.line

            # Return the signal
            return sig
